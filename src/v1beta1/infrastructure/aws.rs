use std::collections::BTreeMap;

use super::*;

pub use machine::AWSMachineTemplate;
pub use network::ClassicELBScheme;
pub use network::NetworkSpec;
pub use network::NetworkStatus;
pub use network::VPCSpec;
pub use tags::Tags;
pub use types::AMIReference;
pub use types::AZSelectionScheme;
pub use types::EKSAMILookupType;
pub use types::Instance;
pub use types::InstanceState;
pub use types::Volume;
pub use types::VolumeType;

mod impls;
mod machine;
mod network;
mod tags;
mod types;

/// AWSClusterSpec defines the desired state of an EC2-based Kubernetes cluster.
#[skip_serializing_none]
#[derive(CustomResource, Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "infrastructure.cluster.x-k8s.io",
    version = "v1beta1",
    kind = "AWSCluster",
    plural = "awsclusters",
    status = "AWSClusterStatus"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct AWSClusterSpec {
    /// NetworkSpec encapsulates all things related to AWS network.
    pub network: Option<NetworkSpec>,

    /// The AWS Region the cluster lives in.
    pub region: Option<String>,

    /// SSHKeyName is the name of the ssh key to attach to the bastion host. Valid values are empty string (do not use SSH keys), a valid SSH key name, or omitted (use the default SSH key name)
    // +optional
    pub ssh_key_name: Option<String>,

    /// ControlPlaneEndpoint represents the endpoint used to communicate with the control plane.
    // +optional
    pub control_plane_endpoint: Option<clusterv1::ApiEndpoint>,

    /// AdditionalTags is an optional set of tags to add to AWS resources managed by the AWS provider, in addition to the
    /// ones added by default.
    // +optional
    pub additional_tags: Option<Tags>,

    /// ControlPlaneLoadBalancer is optional configuration for customizing control plane behavior.
    // +optional
    pub control_plane_load_balancer: Option<AWSLoadBalancerSpec>,

    /// ImageLookupFormat is the AMI naming format to look up machine images when
    /// a machine does not specify an AMI. When set, this will be used for all
    /// cluster machines unless a machine specifies a different ImageLookupOrg.
    /// Supports substitutions for {{.BaseOS}} and {{.K8sVersion}} with the base
    /// OS and kubernetes version, respectively. The BaseOS will be the value in
    /// ImageLookupBaseOS or ubuntu (the default), and the kubernetes version as
    /// defined by the packages produced by kubernetes/release without v as a
    /// prefix: 1.13.0, 1.12.5-mybuild.1, or 1.17.3. For example, the default
    /// image format of capa-ami-{{.BaseOS}}-?{{.K8sVersion}}-* will end up
    /// searching for AMIs that match the pattern capa-ami-ubuntu-?1.18.0-* for a
    /// Machine that is targeting kubernetes v1.18.0 and the ubuntu base OS. See
    /// also: https://golang.org/pkg/text/template/
    // +optional
    pub image_lookup_format: Option<String>,

    /// ImageLookupOrg is the AWS Organization ID to look up machine images when a
    /// machine does not specify an AMI. When set, this will be used for all
    /// cluster machines unless a machine specifies a different ImageLookupOrg.
    // +optional
    pub image_lookup_org: Option<String>,

    /// ImageLookupBaseOS is the name of the base operating system used to look
    /// up machine images when a machine does not specify an AMI. When set, this
    /// will be used for all cluster machines unless a machine specifies a
    /// different ImageLookupBaseOS.
    #[serde(rename = "imageLookupBaseOS")]
    pub image_lookup_base_os: Option<String>,

    /// Bastion contains options to configure the bastion host.
    // +optional
    pub bastion: Option<Bastion>,

    /// IdentityRef is a reference to a identity to be used when reconciling this cluster
    // +optional
    pub identity_ref: Option<AWSIdentityReference>,
}

/// AWSClusterStatus defines the observed state of AWSCluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AWSClusterStatus {
    // +kubebuilder:default=false
    pub ready: bool,
    pub network_status: Option<NetworkStatus>,
    pub failure_domains: Option<clusterv1::FailureDomains>,
    pub bastion: Option<Instance>,
    pub conditions: Option<clusterv1::Conditions>,
}

/// AWSIdentityKind defines allowed AWS identity types.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum AWSIdentityKind {
    AWSClusterControllerIdentity,
    AWSClusterRoleIdentity,
    AWSClusterStaticIdentity,
}

/// ControllerIdentityKind defines identity reference kind as AWSClusterControllerIdentity.
#[allow(non_upper_case_globals)]
pub const ControllerIdentityKind: AWSIdentityKind = AWSIdentityKind::AWSClusterControllerIdentity;

/// ClusterRoleIdentityKind defines identity reference kind as AWSClusterRoleIdentity.
#[allow(non_upper_case_globals)]
pub const ClusterRoleIdentityKind: AWSIdentityKind = AWSIdentityKind::AWSClusterRoleIdentity;

/// ClusterStaticIdentityKind defines identity reference kind as AWSClusterStaticIdentity.
#[allow(non_upper_case_globals)]
pub const ClusterStaticIdentityKind: AWSIdentityKind = AWSIdentityKind::AWSClusterStaticIdentity;

/// AWSIdentityReference specifies a identity.
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AWSIdentityReference {
    /// Name of the identity.
    // +kubebuilder:validation:MinLength=1
    pub name: String,

    /// Kind of the identity.
    // +kubebuilder:validation:Enum=AWSClusterControllerIdentity;AWSClusterRoleIdentity;AWSClusterStaticIdentity
    pub kind: AWSIdentityKind,
}

// Bastion defines a bastion host.
#[skip_serializing_none]
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct Bastion {
    /// Enabled allows this provider to create a bastion host instance
    /// with a public ip to access the VPC private network.
    // +optional
    pub enabled: Option<bool>,

    /// DisableIngressRules will ensure there are no Ingress rules in the bastion host's security group.
    /// Requires AllowedCIDRBlocks to be empty.
    // +optional
    pub disable_ingress_rules: Option<bool>,

    /// AllowedCIDRBlocks is a list of CIDR blocks allowed to access the bastion host.
    /// They are set as ingress rules for the Bastion host's Security Group (defaults to 0.0.0.0/0).
    // +optional
    #[serde(
        default,
        rename = "allowedCIDRBlocks",
        skip_serializing_if = "Vec::is_empty"
    )]
    pub allowed_cidr_blocks: Vec<String>,

    /// InstanceType will use the specified instance type for the bastion. If not specified,
    /// Cluster API Provider AWS will use t3.micro for all regions except us-east-1, where t2.micro
    /// will be the default.
    pub instance_type: Option<String>,

    /// AMI will use the specified AMI to boot the bastion. If not specified,
    /// the AMI will default to one picked out in public space.
    // +optional
    pub ami: Option<String>,
}

/// AWSLoadBalancerSpec defines the desired state of an AWS load balancer.
#[skip_serializing_none]
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AWSLoadBalancerSpec {
    /// Name sets the name of the classic ELB load balancer. As per AWS, the name must be unique
    /// within your set of load balancers for the region, must have a maximum of 32 characters, must
    /// contain only alphanumeric characters or hyphens, and cannot begin or end with a hyphen. Once
    /// set, the value cannot be changed.
    /// +kubebuilder:validation:MaxLength:=32
    /// +kubebuilder:validation:Pattern=`^[A-Za-z0-9]([A-Za-z0-9]{0,31}|[-A-Za-z0-9]{0,30}[A-Za-z0-9])$`
    // +optional
    pub name: Option<String>,

    /// Scheme sets the scheme of the load balancer (defaults to internet-facing)
    /// +kubebuilder:default=internet-facing
    /// +kubebuilder:validation:Enum=internet-facing;internal
    // +optional
    pub scheme: Option<ClassicELBScheme>,

    /// CrossZoneLoadBalancing enables the classic ELB cross availability zone balancing.
    ///
    /// With cross-zone load balancing, each load balancer node for your Classic Load Balancer
    /// distributes requests evenly across the registered instances in all enabled Availability Zones.
    /// If cross-zone load balancing is disabled, each load balancer node distributes requests evenly across
    /// the registered instances in its Availability Zone only.
    ///
    /// Defaults to false.
    // +optional
    pub cross_zone_load_balancing: Option<bool>, // `json:"crossZoneLoadBalancing"`

    /// Subnets sets the subnets that should be applied to the control plane load balancer (defaults to discovered subnets for managed VPCs or an empty set for unmanaged VPCs)
    // +optional
    pub subnets: Option<Vec<String>>, // `json:"subnets,omitempty"`

    /// AdditionalSecurityGroups sets the security groups used by the load balancer. Expected to be security group IDs
    /// This is optional - if not provided new security groups will be created for the load balancer
    // +optional
    pub additional_security_groups: Option<Vec<String>>, // `json:"additionalSecurityGroups,omitempty"`
}

#[cfg(test)]
mod tests;

/* ==========================================================================

package v1beta1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

const (
    // ClusterFinalizer allows ReconcileAWSCluster to clean up AWS resources associated with AWSCluster before
    // removing it from the apiserver.
    ClusterFinalizer = "awscluster.infrastructure.cluster.x-k8s.io"

    // AWSClusterControllerIdentityName is the name of the AWSClusterControllerIdentity singleton.
    AWSClusterControllerIdentityName = "default"
)



// +kubebuilder:object:root=true
// +kubebuilder:resource:path=awsclusters,scope=Namespaced,categories=cluster-api,shortName=awsc
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Cluster",type="string",JSONPath=".metadata.labels.cluster\\.x-k8s\\.io/cluster-name",description="Cluster to which this AWSCluster belongs"
// +kubebuilder:printcolumn:name="Ready",type="string",JSONPath=".status.ready",description="Cluster infrastructure is ready for EC2 instances"
// +kubebuilder:printcolumn:name="VPC",type="string",JSONPath=".spec.network.vpc.id",description="AWS VPC the cluster is using"
// +kubebuilder:printcolumn:name="Endpoint",type="string",JSONPath=".spec.controlPlaneEndpoint",description="API Endpoint",priority=1
// +kubebuilder:printcolumn:name="Bastion IP",type="string",JSONPath=".status.bastion.publicIp",description="Bastion IP address for breakglass access"
// +k8s:defaulter-gen=true

// AWSCluster is the schema for Amazon EC2 based Kubernetes Cluster API.
type AWSCluster struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec   AWSClusterSpec   `json:"spec,omitempty"`
    Status AWSClusterStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// AWSClusterList contains a list of AWSCluster.
// +k8s:defaulter-gen=true
type AWSClusterList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []AWSCluster `json:"items"`
}

// GetConditions returns the observations of the operational state of the AWSCluster resource.
func (r *AWSCluster) GetConditions() clusterv1.Conditions {
    return r.Status.Conditions
}

// SetConditions sets the underlying service state of the AWSCluster to the predescribed clusterv1.Conditions.
func (r *AWSCluster) SetConditions(conditions clusterv1.Conditions) {
    r.Status.Conditions = conditions
}

func init() {
    SchemeBuilder.Register(&AWSCluster{}, &AWSClusterList{})
}

========================================================================== */
