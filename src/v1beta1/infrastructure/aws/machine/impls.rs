use super::*;

impl AWSMachineTemplate {
    pub fn with_type(name: &str, instance_type: &str) -> Self {
        let machine_spec = AWSMachineSpec::with_type(instance_type);
        let template = AWSMachineTemplateResource::new(machine_spec);
        let spec = AWSMachineTemplateSpec::new(template);
        Self::new(name, spec)
    }

    pub fn t3small(name: &str) -> Self {
        Self::with_type(name, "t3.small")
    }

    pub fn m5large(name: &str) -> Self {
        Self::with_type(name, "m5.large")
    }

    pub fn m5xlarge(name: &str) -> Self {
        Self::with_type(name, "m5.xlarge")
    }

    pub fn m5dxlarge(name: &str) -> Self {
        Self::with_type(name, "m5d.xlarge")
    }

    pub fn m5d4xlarge(name: &str) -> Self {
        Self::with_type(name, "m5d.4xlarge")
    }

    pub fn m5d16large(name: &str) -> Self {
        Self::with_type(name, "m5d.16xlarge")
    }

    pub fn m6gdxlarge(name: &str) -> Self {
        Self::with_type(name, "m6gd.xlarge")
    }

    pub fn m6gd4xlarge(name: &str) -> Self {
        Self::with_type(name, "m6gd.4xlarge")
    }

    pub fn m6ilarge(name: &str) -> Self {
        Self::with_type(name, "m6i.large")
    }

    pub fn m6ixlarge(name: &str) -> Self {
        Self::with_type(name, "m6i.xlarge")
    }

    #[must_use]
    pub fn ssh_key_name(mut self, name: &str) -> Self {
        self.spec.template.spec.ssh_key_name = Some(name.to_string());
        self
    }

    #[must_use]
    pub fn iam_instance_profile(mut self, iam_instance_profile: &str) -> Self {
        self.spec.template.spec.iam_instance_profile = Some(iam_instance_profile.to_string());
        self
    }

    #[must_use]
    pub fn public_ip(mut self, yes: bool) -> Self {
        self.spec.template.spec.public_ip = Some(yes);
        self
    }

    #[must_use]
    pub fn ami(mut self, id: &str) -> Self {
        let id = id.to_string();
        let mut ami = self.spec.template.spec.ami.unwrap_or_default();
        ami.id = Some(id);
        self.spec.template.spec.ami = Some(ami);
        self
    }

    #[must_use]
    pub fn root_volume(mut self, root_volume: Volume) -> Self {
        self.spec.template.spec.root_volume = Some(root_volume);
        self
    }

    #[must_use]
    pub fn root_volume_size(self, size: i64) -> Self {
        let root_volume = Volume::gp3(size);
        self.root_volume(root_volume)
    }

    #[must_use]
    pub fn non_root_volumes(mut self, volumes: impl IntoIterator<Item = Volume>) -> Self {
        self.spec.template.spec.non_root_volumes = volumes.into_iter().collect();
        self
    }

    #[must_use]
    pub fn namespace(self, namespace: &str) -> Self {
        Self {
            metadata: kube::core::ObjectMeta {
                namespace: Some(namespace.to_string()),
                ..self.metadata
            },
            ..self
        }
    }

    pub fn additional_security_group(self, arn: &str) -> Self {
        let mut additional_security_groups = self.spec.template.spec.additional_security_groups;
        additional_security_groups.push(types::AWSResourceReference {
            arn: Some(arn.to_string()),
            id: None,
            filters: vec![],
        });
        Self {
            spec: AWSMachineTemplateSpec {
                template: AWSMachineTemplateResource {
                    spec: AWSMachineSpec {
                        additional_security_groups,
                        ..self.spec.template.spec
                    },
                    ..self.spec.template
                },
            },
            ..self
        }
    }
}

impl AWSMachineTemplateSpec {
    fn new(template: AWSMachineTemplateResource) -> Self {
        Self { template }
    }
}

impl AWSMachineTemplateResource {
    fn new(spec: AWSMachineSpec) -> Self {
        Self {
            spec,
            ..Self::default()
        }
    }
}

impl AWSMachineSpec {
    fn with_type(instnce_type: &str) -> Self {
        let instance_type = instnce_type.to_string();
        Self {
            instance_type,
            ..Self::default()
            // provider_id: todo!(),
            // instance_id: todo!(),
            // image_lookup_format: todo!(),
            // image_lookup_org: todo!(),
            // image_lookup_base_os: todo!(),
            // instance_type: todo!(),
            // additional_tags: todo!(),
            // iam_instance_profile: todo!(),
            // public_ip: todo!(),
            // additional_security_groups: todo!(),
            // failure_domain: todo!(),
            // subnet: todo!(),
            // ssh_key_name: todo!(),
            // network_interfaces: todo!(),
            // uncompressed_user_data: todo!(),
            // tenancy: todo!(),
        }
    }

    pub fn ssh_key_name(self, ssh_key_name: impl ToString) -> Self {
        let ssh_key_name = ssh_key_name.to_string().into();
        Self {
            ssh_key_name,
            ..self
        }
    }
}
