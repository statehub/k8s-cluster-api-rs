use super::*;

use kube::ResourceExt;
use serde_json as json;

#[test]
fn de() {
    let cluster: AWSCluster = json::from_str(CLUSTER).unwrap();

    assert_eq!(cluster.name(), "mgmt1");

    let spec = cluster.spec;
    let status = cluster.status.unwrap();

    assert_eq!(spec.region(), Some("us-east-2"));
    assert_eq!(spec.vpc_id(), Some("vpc-08a54407270d43458"));
    assert!(status.ready());

    let cluster: AWSCluster = json::from_str(CLUSTER2).unwrap();

    assert_eq!(cluster.name(), "pop-4");

    let spec = cluster.spec;
    let status = cluster.status.unwrap();

    assert_eq!(spec.region(), Some("us-east-2"));
    assert_eq!(spec.vpc_id(), Some("vpc-0b3042b77678a07c5"));
    assert!(!status.ready());
}

const CLUSTER: &str = r#"
{
    "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
    "kind": "AWSCluster",
    "metadata": {
        "annotations": {
            "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"metadata\":{\"annotations\":{},\"name\":\"mgmt1\",\"namespace\":\"default\"},\"spec\":{\"network\":{\"subnets\":[{\"availabilityZone\":\"us-east-2a\",\"cidrBlock\":\"10.131.0.16/28\",\"isPublic\":true},{\"availabilityZone\":\"us-east-2a\",\"cidrBlock\":\"10.31.0.16/28\"},{\"availabilityZone\":\"us-east-2b\",\"cidrBlock\":\"10.131.0.32/28\",\"isPublic\":true},{\"availabilityZone\":\"us-east-2b\",\"cidrBlock\":\"10.31.0.32/28\"}],\"vpc\":{\"cidrBlock\":\"10.31.0.16/28\"}},\"region\":\"us-east-2\",\"spec\":null,\"sshKeyName\":\"replixio\"}}\n"
        },
        "creationTimestamp": "2021-11-30T13:35:07Z",
        "finalizers": [
            "awscluster.infrastructure.cluster.x-k8s.io"
        ],
        "generation": 10,
        "labels": {
            "cluster.x-k8s.io/cluster-name": "mgmt1"
        },
        "managedFields": [
            {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "fieldsType": "FieldsV1",
                "fieldsV1": {
                    "f:metadata": {
                        "f:labels": {
                            ".": {},
                            "f:cluster.x-k8s.io/cluster-name": {}
                        },
                        "f:ownerReferences": {
                            ".": {},
                            "k:{\"uid\":\"94f05757-dcc1-447c-b020-844301437928\"}": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:blockOwnerDeletion": {},
                                "f:controller": {},
                                "f:kind": {},
                                "f:name": {},
                                "f:uid": {}
                            }
                        }
                    }
                },
                "manager": "manager",
                "operation": "Update",
                "time": "2021-11-30T13:35:31Z"
            },
            {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "fieldsType": "FieldsV1",
                "fieldsV1": {
                    "f:metadata": {
                        "f:annotations": {
                            ".": {},
                            "f:kubectl.kubernetes.io/last-applied-configuration": {}
                        }
                    },
                    "f:spec": {
                        ".": {},
                        "f:network": {
                            ".": {},
                            "f:vpc": {
                                ".": {},
                                "f:availabilityZoneSelection": {},
                                "f:availabilityZoneUsageLimit": {},
                                "f:cidrBlock": {}
                            }
                        },
                        "f:region": {},
                        "f:sshKeyName": {}
                    }
                },
                "manager": "kubectl-client-side-apply",
                "operation": "Update",
                "time": "2021-11-30T13:36:55Z"
            },
            {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "fieldsType": "FieldsV1",
                "fieldsV1": {
                    "f:metadata": {
                        "f:finalizers": {
                            ".": {},
                            "v:\"awscluster.infrastructure.cluster.x-k8s.io\"": {}
                        }
                    },
                    "f:spec": {
                        "f:controlPlaneEndpoint": {
                            "f:host": {},
                            "f:port": {}
                        },
                        "f:controlPlaneLoadBalancer": {
                            "f:name": {}
                        },
                        "f:network": {
                            "f:subnets": {},
                            "f:vpc": {
                                "f:id": {},
                                "f:internetGatewayId": {},
                                "f:tags": {
                                    ".": {},
                                    "f:Name": {},
                                    "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                    "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                }
                            }
                        }
                    },
                    "f:status": {
                        ".": {},
                        "f:conditions": {},
                        "f:failureDomains": {
                            ".": {},
                            "f:us-east-2a": {
                                ".": {},
                                "f:controlPlane": {}
                            },
                            "f:us-east-2b": {
                                ".": {},
                                "f:controlPlane": {}
                            }
                        },
                        "f:networkStatus": {
                            ".": {},
                            "f:apiServerElb": {
                                ".": {},
                                "f:attributes": {
                                    ".": {},
                                    "f:idleTimeout": {}
                                },
                                "f:availabilityZones": {},
                                "f:dnsName": {},
                                "f:name": {},
                                "f:scheme": {},
                                "f:securityGroupIds": {},
                                "f:subnetIds": {},
                                "f:tags": {
                                    ".": {},
                                    "f:Name": {},
                                    "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                    "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                }
                            },
                            "f:securityGroups": {
                                ".": {},
                                "f:apiserver-lb": {
                                    ".": {},
                                    "f:id": {},
                                    "f:ingressRule": {},
                                    "f:name": {},
                                    "f:tags": {
                                        ".": {},
                                        "f:Name": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                    }
                                },
                                "f:bastion": {
                                    ".": {},
                                    "f:id": {},
                                    "f:ingressRule": {},
                                    "f:name": {},
                                    "f:tags": {
                                        ".": {},
                                        "f:Name": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                    }
                                },
                                "f:controlplane": {
                                    ".": {},
                                    "f:id": {},
                                    "f:ingressRule": {},
                                    "f:name": {},
                                    "f:tags": {
                                        ".": {},
                                        "f:Name": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                    }
                                },
                                "f:lb": {
                                    ".": {},
                                    "f:id": {},
                                    "f:name": {},
                                    "f:tags": {
                                        ".": {},
                                        "f:Name": {},
                                        "f:kubernetes.io/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                    }
                                },
                                "f:node": {
                                    ".": {},
                                    "f:id": {},
                                    "f:ingressRule": {},
                                    "f:name": {},
                                    "f:tags": {
                                        ".": {},
                                        "f:Name": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": {},
                                        "f:sigs.k8s.io/cluster-api-provider-aws/role": {}
                                    }
                                }
                            }
                        },
                        "f:ready": {}
                    }
                },
                "manager": "cluster-api-provider-aws-controller",
                "operation": "Update",
                "time": "2021-11-30T13:40:03Z"
            }
        ],
        "name": "mgmt1",
        "namespace": "default",
        "ownerReferences": [
            {
                "apiVersion": "cluster.x-k8s.io/v1beta1",
                "blockOwnerDeletion": true,
                "controller": true,
                "kind": "Cluster",
                "name": "mgmt1",
                "uid": "94f05757-dcc1-447c-b020-844301437928"
            }
        ],
        "resourceVersion": "6016434",
        "uid": "1a15a32f-e138-4708-b6ed-3e168d5aaed9"
    },
    "spec": {
        "bastion": {
            "allowedCIDRBlocks": [
                "0.0.0.0/0"
            ],
            "enabled": false
        },
        "controlPlaneEndpoint": {
            "host": "mgmt1-apiserver-1759940877.us-east-2.elb.amazonaws.com",
            "port": 6443
        },
        "controlPlaneLoadBalancer": {
            "crossZoneLoadBalancing": false,
            "name": "mgmt1-apiserver",
            "scheme": "internet-facing"
        },
        "identityRef": {
            "kind": "AWSClusterControllerIdentity",
            "name": "default"
        },
        "network": {
            "cni": {
                "cniIngressRules": [
                    {
                        "description": "bgp (calico)",
                        "fromPort": 179,
                        "protocol": "tcp",
                        "toPort": 179
                    },
                    {
                        "description": "IP-in-IP (calico)",
                        "fromPort": -1,
                        "protocol": "4",
                        "toPort": 65535
                    }
                ]
            },
            "subnets": [
                {
                    "availabilityZone": "us-east-2a",
                    "cidrBlock": "10.131.0.16/28",
                    "id": "subnet-0deee7de4836654fa",
                    "isPublic": true,
                    "natGatewayId": "nat-003e4000e60069d07",
                    "routeTableId": "rtb-0b56b7439928346ba",
                    "tags": {
                        "Name": "mgmt1-subnet-public-us-east-2a",
                        "kubernetes.io/cluster/mgmt1": "shared",
                        "kubernetes.io/role/elb": "1",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "public"
                    }
                },
                {
                    "availabilityZone": "us-east-2a",
                    "cidrBlock": "10.31.0.16/28",
                    "id": "subnet-0687a1c194b0aaa50",
                    "isPublic": false,
                    "routeTableId": "rtb-0581eefef46110254",
                    "tags": {
                        "Name": "mgmt1-subnet-private-us-east-2a",
                        "kubernetes.io/cluster/mgmt1": "shared",
                        "kubernetes.io/role/internal-elb": "1",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "private"
                    }
                },
                {
                    "availabilityZone": "us-east-2b",
                    "cidrBlock": "10.131.0.32/28",
                    "id": "subnet-09326addd5efd5a0d",
                    "isPublic": true,
                    "natGatewayId": "nat-0c81800b71d808a5e",
                    "routeTableId": "rtb-0d5155764b77a8601",
                    "tags": {
                        "Name": "mgmt1-subnet-public-us-east-2b",
                        "kubernetes.io/cluster/mgmt1": "shared",
                        "kubernetes.io/role/elb": "1",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "public"
                    }
                },
                {
                    "availabilityZone": "us-east-2b",
                    "cidrBlock": "10.31.0.32/28",
                    "id": "subnet-06b72c501e03fc7b1",
                    "isPublic": false,
                    "routeTableId": "rtb-0f3e74340c496c9fd",
                    "tags": {
                        "Name": "mgmt1-subnet-private-us-east-2b",
                        "kubernetes.io/cluster/mgmt1": "shared",
                        "kubernetes.io/role/internal-elb": "1",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "private"
                    }
                }
            ],
            "vpc": {
                "availabilityZoneSelection": "Ordered",
                "availabilityZoneUsageLimit": 3,
                "cidrBlock": "10.31.0.16/28",
                "id": "vpc-08a54407270d43458",
                "internetGatewayId": "igw-0f36a4c3702f2f39a",
                "tags": {
                    "Name": "mgmt1-vpc",
                    "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                    "sigs.k8s.io/cluster-api-provider-aws/role": "common"
                }
            }
        },
        "region": "us-east-2",
        "sshKeyName": "replixio"
    },
    "status": {
        "conditions": [
            {
                "lastTransitionTime": "2021-11-30T13:40:03Z",
                "status": "True",
                "type": "Ready"
            },
            {
                "lastTransitionTime": "2021-11-30T13:38:55Z",
                "status": "True",
                "type": "ClusterSecurityGroupsReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:36:47Z",
                "status": "True",
                "type": "InternetGatewayReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:40:03Z",
                "status": "True",
                "type": "LoadBalancerReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:38:50Z",
                "status": "True",
                "type": "NatGatewaysReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:35:32Z",
                "status": "True",
                "type": "PrincipalUsageAllowed"
            },
            {
                "lastTransitionTime": "2021-11-30T13:38:56Z",
                "status": "True",
                "type": "RouteTablesReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:36:47Z",
                "status": "True",
                "type": "SubnetsReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:35:33Z",
                "status": "True",
                "type": "VpcReady"
            }
        ],
        "failureDomains": {
            "us-east-2a": {
                "controlPlane": true
            },
            "us-east-2b": {
                "controlPlane": true
            }
        },
        "networkStatus": {
            "apiServerElb": {
                "attributes": {
                    "idleTimeout": 600000000000
                },
                "availabilityZones": [
                    "us-east-2a",
                    "us-east-2b"
                ],
                "dnsName": "mgmt1-apiserver-1759940877.us-east-2.elb.amazonaws.com",
                "name": "mgmt1-apiserver",
                "scheme": "internet-facing",
                "securityGroupIds": [
                    "sg-0369a50389eff6574"
                ],
                "subnetIds": [
                    "subnet-09326addd5efd5a0d",
                    "subnet-0deee7de4836654fa"
                ],
                "tags": {
                    "Name": "mgmt1-apiserver",
                    "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                    "sigs.k8s.io/cluster-api-provider-aws/role": "apiserver"
                }
            },
            "securityGroups": {
                "apiserver-lb": {
                    "id": "sg-0369a50389eff6574",
                    "ingressRule": [
                        {
                            "cidrBlocks": [
                                "0.0.0.0/0"
                            ],
                            "description": "Kubernetes API",
                            "fromPort": 6443,
                            "protocol": "tcp",
                            "toPort": 6443
                        }
                    ],
                    "name": "mgmt1-apiserver-lb",
                    "tags": {
                        "Name": "mgmt1-apiserver-lb",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "apiserver-lb"
                    }
                },
                "bastion": {
                    "id": "sg-06e1bc3f0407f6a5c",
                    "ingressRule": [
                        {
                            "cidrBlocks": [
                                "0.0.0.0/0"
                            ],
                            "description": "SSH",
                            "fromPort": 22,
                            "protocol": "tcp",
                            "toPort": 22
                        }
                    ],
                    "name": "mgmt1-bastion",
                    "tags": {
                        "Name": "mgmt1-bastion",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "bastion"
                    }
                },
                "controlplane": {
                    "id": "sg-03a12eda3c3ecddb8",
                    "ingressRule": [
                        {
                            "description": "Kubernetes API",
                            "fromPort": 6443,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-0369a50389eff6574",
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 6443
                        },
                        {
                            "description": "SSH",
                            "fromPort": 22,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-06e1bc3f0407f6a5c"
                            ],
                            "toPort": 22
                        },
                        {
                            "description": "etcd",
                            "fromPort": 2379,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8"
                            ],
                            "toPort": 2379
                        },
                        {
                            "description": "etcd peer",
                            "fromPort": 2380,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8"
                            ],
                            "toPort": 2380
                        },
                        {
                            "description": "bgp (calico)",
                            "fromPort": 179,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 179
                        },
                        {
                            "description": "IP-in-IP (calico)",
                            "fromPort": 0,
                            "protocol": "4",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 0
                        }
                    ],
                    "name": "mgmt1-controlplane",
                    "tags": {
                        "Name": "mgmt1-controlplane",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "controlplane"
                    }
                },
                "lb": {
                    "id": "sg-07b5b06c3ad92061c",
                    "name": "mgmt1-lb",
                    "tags": {
                        "Name": "mgmt1-lb",
                        "kubernetes.io/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "lb"
                    }
                },
                "node": {
                    "id": "sg-0e0e4ba71a3ddd42e",
                    "ingressRule": [
                        {
                            "cidrBlocks": [
                                "0.0.0.0/0"
                            ],
                            "description": "Node Port Services",
                            "fromPort": 30000,
                            "protocol": "tcp",
                            "toPort": 32767
                        },
                        {
                            "description": "SSH",
                            "fromPort": 22,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-06e1bc3f0407f6a5c"
                            ],
                            "toPort": 22
                        },
                        {
                            "description": "Kubelet API",
                            "fromPort": 10250,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 10250
                        },
                        {
                            "description": "bgp (calico)",
                            "fromPort": 179,
                            "protocol": "tcp",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 179
                        },
                        {
                            "description": "IP-in-IP (calico)",
                            "fromPort": 0,
                            "protocol": "4",
                            "sourceSecurityGroupIds": [
                                "sg-03a12eda3c3ecddb8",
                                "sg-0e0e4ba71a3ddd42e"
                            ],
                            "toPort": 0
                        }
                    ],
                    "name": "mgmt1-node",
                    "tags": {
                        "Name": "mgmt1-node",
                        "sigs.k8s.io/cluster-api-provider-aws/cluster/mgmt1": "owned",
                        "sigs.k8s.io/cluster-api-provider-aws/role": "node"
                    }
                }
            }
        },
        "ready": true
    }
}
"#;

const CLUSTER2: &str = r#"
{"apiVersion":"infrastructure.cluster.x-k8s.io/v1beta1","kind":"AWSCluster","metadata":{"creationTimestamp":"2021-12-12T14:09:02Z","deletionGracePeriodSeconds":0,"deletionTimestamp":"2021-12-12T16:47:52Z","finalizers":["awscluster.infrastructure.cluster.x-k8s.io"],"generation":7,"labels":{"cluster.x-k8s.io/cluster-name":"pop-4"},"managedFields":[{"apiVersion":"infrastructure.cluster.x-k8s.io/v1beta1","fieldsType":"FieldsV1","fieldsV1":{"f:metadata":{"f:labels":{".":{},"f:cluster.x-k8s.io/cluster-name":{}},"f:ownerReferences":{".":{},"k:{\"uid\":\"f32a17d4-95b4-4dcc-916d-116ae39f6181\"}":{".":{},"f:apiVersion":{},"f:blockOwnerDeletion":{},"f:controller":{},"f:kind":{},"f:name":{},"f:uid":{}}}}},"manager":"manager","operation":"Update","time":"2021-12-12T14:09:02Z"},{"apiVersion":"infrastructure.cluster.x-k8s.io/v1beta1","fieldsType":"FieldsV1","fieldsV1":{"f:spec":{".":{},"f:network":{".":{},"f:vpc":{".":{},"f:availabilityZoneSelection":{},"f:availabilityZoneUsageLimit":{},"f:cidrBlock":{}}},"f:region":{},"f:sshKeyName":{}}},"manager":"unknown","operation":"Update","time":"2021-12-12T14:09:02Z"},{"apiVersion":"infrastructure.cluster.x-k8s.io/v1beta1","fieldsType":"FieldsV1","fieldsV1":{"f:metadata":{"f:finalizers":{".":{},"v:\"awscluster.infrastructure.cluster.x-k8s.io\"":{}}},"f:spec":{"f:controlPlaneLoadBalancer":{"f:name":{}},"f:network":{"f:subnets":{},"f:vpc":{"f:id":{},"f:internetGatewayId":{},"f:tags":{".":{},"f:Name":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}}}},"f:status":{".":{},"f:conditions":{},"f:networkStatus":{".":{},"f:securityGroups":{".":{},"f:apiserver-lb":{".":{},"f:id":{},"f:ingressRule":{},"f:name":{},"f:tags":{".":{},"f:Name":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}},"f:bastion":{".":{},"f:id":{},"f:ingressRule":{},"f:name":{},"f:tags":{".":{},"f:Name":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}},"f:controlplane":{".":{},"f:id":{},"f:ingressRule":{},"f:name":{},"f:tags":{".":{},"f:Name":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}},"f:lb":{".":{},"f:id":{},"f:name":{},"f:tags":{".":{},"f:Name":{},"f:kubernetes.io/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}},"f:node":{".":{},"f:id":{},"f:ingressRule":{},"f:name":{},"f:tags":{".":{},"f:Name":{},"f:sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":{},"f:sigs.k8s.io/cluster-api-provider-aws/role":{}}}}},"f:ready":{}}},"manager":"cluster-api-provider-aws-controller","operation":"Update","time":"2021-12-12T14:50:02Z"}],"name":"pop-4","namespace":"default","ownerReferences":[{"apiVersion":"cluster.x-k8s.io/v1beta1","blockOwnerDeletion":true,"controller":true,"kind":"Cluster","name":"pop-4","uid":"f32a17d4-95b4-4dcc-916d-116ae39f6181"}],"resourceVersion":"13536672","uid":"4d5fdbf5-6d94-4a4c-bd79-3492eb67e769"},"spec":{"bastion":{"allowedCIDRBlocks":["0.0.0.0/0"],"enabled":false},"controlPlaneEndpoint":{"host":"","port":0},"controlPlaneLoadBalancer":{"crossZoneLoadBalancing":false,"name":"pop-4-apiserver","scheme":"internet-facing"},"identityRef":{"kind":"AWSClusterControllerIdentity","name":"default"},"network":{"cni":{"cniIngressRules":[{"description":"bgp (calico)","fromPort":179,"protocol":"tcp","toPort":179},{"description":"IP-in-IP (calico)","fromPort":-1,"protocol":"4","toPort":65535}]},"subnets":[{"availabilityZone":"us-east-2a","cidrBlock":"10.16.0.176/28","id":"subnet-0ac17fa43448227e8","isPublic":false,"routeTableId":"rtb-0d0cca26620a5c7ad","tags":{"Name":"pop-4-subnet-private-us-east-2a","kubernetes.io/cluster/pop-4":"shared","kubernetes.io/role/internal-elb":"1","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"private"}},{"availabilityZone":"us-east-2a","cidrBlock":"10.112.0.176/28","id":"subnet-00031783e1364e0ab","isPublic":true,"natGatewayId":"nat-03fedf0f955bb12ad","routeTableId":"rtb-0b1aac902841ace40","tags":{"Name":"pop-4-subnet-public-us-east-2a","kubernetes.io/cluster/pop-4":"shared","kubernetes.io/role/elb":"1","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"public"}},{"availabilityZone":"us-east-2b","cidrBlock":"10.16.0.224/28","id":"subnet-0a22a51044ad4af6b","isPublic":false,"routeTableId":"rtb-04aa6dacdfdc0cd22","tags":{"Name":"pop-4-subnet-private-us-east-2b","kubernetes.io/cluster/pop-4":"shared","kubernetes.io/role/internal-elb":"1","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"private"}},{"availabilityZone":"us-east-2b","cidrBlock":"10.112.0.224/28","id":"subnet-072ad2329c9c44860","isPublic":true,"natGatewayId":"nat-012e61190dcf1712a","routeTableId":"rtb-0bdc38a698beb4994","tags":{"Name":"pop-4-subnet-public-us-east-2b","kubernetes.io/cluster/pop-4":"shared","kubernetes.io/role/elb":"1","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"public"}}],"vpc":{"availabilityZoneSelection":"Ordered","availabilityZoneUsageLimit":3,"cidrBlock":"10.16.0.176/28","id":"vpc-0b3042b77678a07c5","internetGatewayId":"igw-076a132e3aee83651","tags":{"Name":"pop-4-vpc","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"common"}}},"region":"us-east-2","sshKeyName":"replixio"},"status":{"conditions":[{"lastTransitionTime":"2021-12-12T14:50:00Z","message":"6 of 7 completed","reason":"LoadBalancerFailed","severity":"Error","status":"False","type":"Ready"},{"lastTransitionTime":"2021-12-12T14:50:00Z","status":"True","type":"ClusterSecurityGroupsReady"},{"lastTransitionTime":"2021-12-12T14:47:51Z","status":"True","type":"InternetGatewayReady"},{"lastTransitionTime":"2021-12-12T14:50:00Z","message":"ELB names must be unique within a region: \"pop-4-apiserver\" ELB already exists in this region in VPC \"vpc-0620cab7b71baa197\"","reason":"LoadBalancerFailed","severity":"Error","status":"False","type":"LoadBalancerReady"},{"lastTransitionTime":"2021-12-12T14:49:54Z","status":"True","type":"NatGatewaysReady"},{"lastTransitionTime":"2021-12-12T14:09:02Z","status":"True","type":"PrincipalCredentialRetrieved"},{"lastTransitionTime":"2021-12-12T14:09:02Z","status":"True","type":"PrincipalUsageAllowed"},{"lastTransitionTime":"2021-12-12T14:50:00Z","status":"True","type":"RouteTablesReady"},{"lastTransitionTime":"2021-12-12T14:47:51Z","status":"True","type":"SubnetsReady"},{"lastTransitionTime":"2021-12-12T14:09:04Z","status":"True","type":"VpcReady"}],"networkStatus":{"securityGroups":{"apiserver-lb":{"id":"sg-032c0a432b8cff3d4","ingressRule":[{"cidrBlocks":["0.0.0.0/0"],"description":"Kubernetes API","fromPort":6443,"protocol":"tcp","toPort":6443}],"name":"pop-4-apiserver-lb","tags":{"Name":"pop-4-apiserver-lb","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"apiserver-lb"}},"bastion":{"id":"sg-034792c155c67dbfc","ingressRule":[{"cidrBlocks":["0.0.0.0/0"],"description":"SSH","fromPort":22,"protocol":"tcp","toPort":22}],"name":"pop-4-bastion","tags":{"Name":"pop-4-bastion","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"bastion"}},"controlplane":{"id":"sg-0b584232ed267ef57","ingressRule":[{"description":"Kubernetes API","fromPort":6443,"protocol":"tcp","sourceSecurityGroupIds":["sg-032c0a432b8cff3d4","sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":6443},{"description":"SSH","fromPort":22,"protocol":"tcp","sourceSecurityGroupIds":["sg-034792c155c67dbfc"],"toPort":22},{"description":"etcd","fromPort":2379,"protocol":"tcp","sourceSecurityGroupIds":["sg-0b584232ed267ef57"],"toPort":2379},{"description":"etcd peer","fromPort":2380,"protocol":"tcp","sourceSecurityGroupIds":["sg-0b584232ed267ef57"],"toPort":2380},{"description":"bgp (calico)","fromPort":179,"protocol":"tcp","sourceSecurityGroupIds":["sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":179},{"description":"IP-in-IP (calico)","fromPort":0,"protocol":"4","sourceSecurityGroupIds":["sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":0}],"name":"pop-4-controlplane","tags":{"Name":"pop-4-controlplane","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"controlplane"}},"lb":{"id":"sg-09d733b4bd84a4eac","name":"pop-4-lb","tags":{"Name":"pop-4-lb","kubernetes.io/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"lb"}},"node":{"id":"sg-0b23c5d3388b35d3f","ingressRule":[{"cidrBlocks":["0.0.0.0/0"],"description":"Node Port Services","fromPort":30000,"protocol":"tcp","toPort":32767},{"description":"SSH","fromPort":22,"protocol":"tcp","sourceSecurityGroupIds":["sg-034792c155c67dbfc"],"toPort":22},{"description":"Kubelet API","fromPort":10250,"protocol":"tcp","sourceSecurityGroupIds":["sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":10250},{"description":"bgp (calico)","fromPort":179,"protocol":"tcp","sourceSecurityGroupIds":["sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":179},{"description":"IP-in-IP (calico)","fromPort":0,"protocol":"4","sourceSecurityGroupIds":["sg-0b23c5d3388b35d3f","sg-0b584232ed267ef57"],"toPort":0}],"name":"pop-4-node","tags":{"Name":"pop-4-node","sigs.k8s.io/cluster-api-provider-aws/cluster/pop-4":"owned","sigs.k8s.io/cluster-api-provider-aws/role":"node"}}}},"ready":false}}
"#;
