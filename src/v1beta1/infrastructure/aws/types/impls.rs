use super::*;

impl Volume {
    pub fn gp3(size: i64) -> Self {
        Self {
            size,
            r#type: VolumeType::Gp3,
            ..Self::default()
        }
    }

    pub fn io2(size: i64, iops: i64) -> Self {
        Self {
            size,
            iops: Some(iops),
            r#type: VolumeType::Io2,
            ..Self::default()
        }
    }

    pub fn device_name(self, device_name: impl ToString) -> Self {
        Self {
            device_name: Some(device_name.to_string()),
            ..self
        }
    }
}

impl Default for VolumeType {
    fn default() -> Self {
        Self::Gp3
    }
}
