use super::*;

mod impls;

/// Instance describes an AWS instance.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Instance {
    pub id: String, // `json:"id"`

    /// The current state of the instance.
    pub instance_state: Option<InstanceState>, // `json:"instanceState,omitempty"`

    /// The instance type.
    pub r#type: Option<String>, // `json:"type,omitempty"`

    /// The ID of the subnet of the instance.
    pub subnet_id: Option<String>, // `json:"subnetId,omitempty"`

    /// The ID of the AMI used to launch the instance.
    pub image_id: Option<String>, // `json:"imageId,omitempty"`

    /// The name of the SSH key pair.
    pub ssh_key_name: Option<String>, // `json:"sshKeyName,omitempty"`

    /// SecurityGroupIDs are one or more security group IDs this instance belongs to.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub security_group_ids: Vec<String>, // `json:"securityGroupIds,omitempty"`

    /// UserData is the raw data script passed to the instance which is run upon bootstrap.
    /// This field must not be base64 encoded and should only be used when running a new instance.
    pub user_data: Option<String>, // `json:"userData,omitempty"`

    /// The name of the IAM instance profile associated with the instance, if applicable.
    pub iam_profile: Option<String>, // `json:"iamProfile,omitempty"`

    /// Addresses contains the AWS instance associated addresses.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub addresses: Vec<clusterv1::MachineAddress>, // `json:"addresses,omitempty"`

    /// The private IPv4 address assigned to the instance.
    pub private_ip: Option<String>, // `json:"privateIp,omitempty"`

    /// The public IPv4 address assigned to the instance, if applicable.
    pub public_ip: Option<String>, // `json:"publicIp,omitempty"`

    /// Specifies whether enhanced networking with ENA is enabled.
    pub ena_support: Option<bool>, // `json:"enaSupport,omitempty"`

    /// Indicates whether the instance is optimized for Amazon EBS I/O.
    pub ebs_optimized: Option<bool>, // `json:"ebsOptimized,omitempty"`

    /// Configuration options for the root storage volume.
    // +optional
    pub root_volume: Option<Volume>, // `json:"rootVolume,omitempty"`

    /// Configuration options for the non root storage volumes.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub non_root_volumes: Vec<Volume>, // `json:"nonRootVolumes,omitempty"`

    /// Specifies ENIs attached to instance
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub network_interfaces: Vec<String>, // `json:"networkInterfaces,omitempty"`

    /// The tags associated with the instance.
    #[serde(default, skip_serializing_if = "Tags::is_empty")]
    pub tags: Tags, // `json:"tags,omitempty"`

    /// Availability zone of instance
    pub availability_zone: Option<String>, // `json:"availabilityZone,omitempty"`

    /// SpotMarketOptions option for configuring instances to be run using AWS Spot instances.
    pub spot_market_options: Option<SpotMarketOptions>, // `json:"spotMarketOptions,omitempty"`

    /// Tenancy indicates if instance should run on shared or single-tenant hardware.
    // +optional
    pub tenancy: Option<String>, // `json:"tenancy,omitempty"`

    /// IDs of the instance's volumes
    // +optional
    #[serde(rename = "volumeIDs", default, skip_serializing_if = "Vec::is_empty")]
    pub volume_ids: Vec<String>, // `json:"volumeIDs,omitempty"`
}

/// Volume encapsulates the configuration options for the storage device.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Volume {
    /// Device name
    // +optional
    pub device_name: Option<String>, // `json:"deviceName,omitempty"`

    /// Size specifies size (in Gi) of the storage device.
    /// Must be greater than the image snapshot size or 8 (whichever is greater).
    // +kubebuilder:validation:Minimum=8
    pub size: i64, // `json:"size"`

    /// Type is the type of the volume (e.g. gp2, io1, etc...).
    // +optional
    pub r#type: VolumeType, // `json:"type,omitempty"`

    /// IOPS is the number of IOPS requested for the disk. Not applicable to all types.
    // +optional
    pub iops: Option<i64>, // `json:"iops,omitempty"`

    /// Throughput to provision in MiB/s supported for the volume type. Not applicable to all types.
    // +optional
    pub throughput: Option<i64>, // `json:"throughput,omitempty"`

    /// Encrypted is whether the volume should be encrypted or not.
    // +optional
    pub encrypted: Option<bool>, // `json:"encrypted,omitempty"`

    /// EncryptionKey is the KMS key to use to encrypt the volume. Can be either a KMS key ID or ARN.
    /// If Encrypted is set and this is omitted, the default AWS key will be used.
    /// The key must already exist and be accessible by the controller.
    // +optional
    pub encryption_key: Option<String>, // `json:"encryptionKey,omitempty"`
}

// VolumeType describes the EBS volume type.
// See: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volume-types.html
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum VolumeType {
    /// VolumeTypeIO1 is the string representing a provisioned iops ssd io1 volume.
    Io1,
    /// VolumeTypeIO2 is the string representing a provisioned iops ssd io2 volume.
    Io2,
    /// VolumeTypeGP2 is the string representing a general purpose ssd gp2 volume.
    Gp2,
    /// VolumeTypeGP3 is the string representing a general purpose ssd gp3 volume.
    Gp3,
}

// // VolumeTypesGP are volume types provisioned for general purpose io.
// VolumeTypesGP = sets.NewString(
//     string(VolumeTypeIO1),
//     string(VolumeTypeIO2),
// )

// // VolumeTypesProvisioned are volume types provisioned for high performance io.
// VolumeTypesProvisioned = sets.NewString(
//     string(VolumeTypeIO1),
//     string(VolumeTypeIO2),
// )

/// InstanceState describes the state of an AWS instance.
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum InstanceState {
    /// InstanceStatePending is the string representing an instance in a pending state.
    Pending,

    /// InstanceStateRunning is the string representing an instance in a running state.
    Running,

    /// InstanceStateShuttingDown is the string representing an instance shutting down.
    ShuttingDown,

    /// InstanceStateTerminated is the string representing an instance that has been terminated.
    Terminated,

    /// InstanceStateStopping is the string representing an instance
    /// that is in the process of being stopped and can be restarted.
    Stopping,

    /// InstanceStateStopped is the string representing an instance
    /// that has been stopped and can be restarted.
    Stopped,
}

/// SpotMarketOptions defines the options available to a user when configuring
/// Machines to run on Spot instances.
/// Most users should provide an empty struct.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SpotMarketOptions {
    /// MaxPrice defines the maximum price the user is willing to pay for Spot VM instances
    // +optional
    // +kubebuilder:validation:pattern="^[0-9]+(\.[0-9]+)?$"
    pub max_price: Option<String>, // `json:"maxPrice,omitempty"`
}

// AZSelectionScheme defines the scheme of selecting AZs.
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum AZSelectionScheme {
    Ordered,
    Random,
}

/// AWSResourceReference is a reference to a specific AWS resource by ID, ARN, or filters.
/// Only one of ID, ARN or Filters may be specified. Specifying more than one will result in
/// a validation error.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AWSResourceReference {
    /// ID of resource
    // +optional
    pub id: Option<String>, //`json:"id,omitempty"`

    /// ARN of resource
    // +optional
    pub arn: Option<String>, //`json:"arn,omitempty"`

    /// Filters is a set of key/value pairs used to identify a resource
    /// They are applied according to the rules defined by the AWS API:
    /// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Filtering.html
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub filters: Vec<Filter>, // `json:"filters,omitempty"`
}

/// Filter is a filter used to identify an AWS resource.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Filter {
    /// Name of the filter. Filter names are case-sensitive.
    pub name: String, // `json:"name"`

    /// Values includes one or more filter values. Filter values are case-sensitive.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub values: Vec<String>, // `json:"values"`
}

/// AMIReference is a reference to a specific AWS resource by ID, ARN, or filters.
/// Only one of ID, ARN or Filters may be specified. Specifying more than one will result in
/// a validation error.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AMIReference {
    /// ID of resource
    // +optional
    pub id: Option<String>, // `json:"id,omitempty"`

    /// EKSOptimizedLookupType If specified, will look up an EKS Optimized image in SSM Parameter store
    /// +kubebuilder:validation:Enum:=AmazonLinux;AmazonLinuxGPU
    // +optional
    #[serde(rename = "eksLookupType")]
    pub eks_optimized_lookup_type: Option<EKSAMILookupType>, // `json:"eksLookupType,omitempty"`
}

/// EKSAMILookupType specifies which AWS AMI to use for a AWSMachine and AWSMachinePool.
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum EKSAMILookupType {
    /// AmazonLinux is the default AMI type.
    AmazonLinux,
    /// AmazonLinuxGPU is the AmazonLinux GPU AMI type.
    AmazonLinuxGPU,
}

/*  ============================================================================


package v1beta1

import (
    "k8s.io/apimachinery/pkg/util/sets"
    clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

// AWSResourceReference is a reference to a specific AWS resource by ID, ARN, or filters.
// Only one of ID, ARN or Filters may be specified. Specifying more than one will result in
// a validation error.
type AWSResourceReference struct {
    // ID of resource
    // +optional
    ID *string `json:"id,omitempty"`

    // ARN of resource
    // +optional
    ARN *string `json:"arn,omitempty"`

    // Filters is a set of key/value pairs used to identify a resource
    // They are applied according to the rules defined by the AWS API:
    // https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Using_Filtering.html
    // +optional
    Filters []Filter `json:"filters,omitempty"`
}


// Filter is a filter used to identify an AWS resource.
type Filter struct {
    // Name of the filter. Filter names are case-sensitive.
    Name string `json:"name"`

    // Values includes one or more filter values. Filter values are case-sensitive.
    Values []string `json:"values"`
}

// AWSMachineProviderConditionType is a valid value for AWSMachineProviderCondition.Type.
type AWSMachineProviderConditionType string

// Valid conditions for an AWS machine instance.
const (
    // MachineCreated indicates whether the machine has been created or not. If not,
    // it should include a reason and message for the failure.
    MachineCreated AWSMachineProviderConditionType = "MachineCreated"
)

var (

    // InstanceRunningStates defines the set of states in which an EC2 instance is
    // running or going to be running soon.
    InstanceRunningStates = sets.NewString(
        string(InstanceStatePending),
        string(InstanceStateRunning),
    )

    // InstanceOperationalStates defines the set of states in which an EC2 instance is
    // or can return to running, and supports all EC2 operations.
    InstanceOperationalStates = InstanceRunningStates.Union(
        sets.NewString(
            string(InstanceStateStopping),
            string(InstanceStateStopped),
        ),
    )

    // InstanceKnownStates represents all known EC2 instance states.
    InstanceKnownStates = InstanceOperationalStates.Union(
        sets.NewString(
            string(InstanceStateShuttingDown),
            string(InstanceStateTerminated),
        ),
    )
)




============================================================================ */
