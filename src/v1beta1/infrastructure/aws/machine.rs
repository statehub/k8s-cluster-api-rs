use super::*;

mod impls;

/// AWSMachineTemplateSpec defines the desired state of AWSMachineTemplate.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize, CustomResource)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "infrastructure.cluster.x-k8s.io",
    version = "v1beta1",
    kind = "AWSMachineTemplate",
    plural = "awsmachinetemplates"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct AWSMachineTemplateSpec {
    pub template: AWSMachineTemplateResource, // `json:"template"`
}

/// AWSMachineTemplateResource describes the data needed to create am AWSMachine from a template.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AWSMachineTemplateResource {
    /// Standard object's metadata.
    /// More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
    // +optional
    pub metadata: Option<clusterv1::ObjectMeta>, // `json:"metadata,omitempty"`

    // Spec is the specification of the desired behavior of the machine.
    pub spec: AWSMachineSpec, // `json:"spec"`
}

/// AWSMachineSpec defines the desired state of an Amazon EC2 instance.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize, CustomResource)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "infrastructure.cluster.x-k8s.io",
    version = "v1beta1",
    kind = "AWSMachine",
    plural = "awsmachines",
    status = "AWSMachineStatus"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct AWSMachineSpec {
    /// ProviderID is the unique identifier as specified by the cloud provider.
    #[serde(rename = "providerID")]
    pub provider_id: Option<String>, // `json:"providerID,omitempty"`

    /// InstanceID is the EC2 instance ID for this machine.
    #[serde(rename = "instanceID")]
    pub instance_id: Option<String>, // `json:"instanceID,omitempty"`

    /// AMI is the reference to the AMI from which to create the machine instance.
    pub ami: Option<AMIReference>, // `json:"ami,omitempty"`

    /// ImageLookupFormat is the AMI naming format to look up the image for this
    /// machine It will be ignored if an explicit AMI is set. Supports
    /// substitutions for {{.BaseOS}} and {{.K8sVersion}} with the base OS and
    /// kubernetes version, respectively. The BaseOS will be the value in
    /// ImageLookupBaseOS or ubuntu (the default), and the kubernetes version as
    /// defined by the packages produced by kubernetes/release without v as a
    /// prefix: 1.13.0, 1.12.5-mybuild.1, or 1.17.3. For example, the default
    /// image format of capa-ami-{{.BaseOS}}-?{{.K8sVersion}}-* will end up
    /// searching for AMIs that match the pattern capa-ami-ubuntu-?1.18.0-* for a
    /// Machine that is targeting kubernetes v1.18.0 and the ubuntu base OS. See
    /// also: https://golang.org/pkg/text/template/
    // +optional
    pub image_lookup_format: Option<String>, // `json:"imageLookupFormat,omitempty"`

    /// ImageLookupOrg is the AWS Organization ID to use for image lookup if AMI is not set.
    pub image_lookup_org: Option<String>, // `json:"imageLookupOrg,omitempty"`

    /// ImageLookupBaseOS is the name of the base operating system to use for
    /// image lookup the AMI is not set.
    #[serde(rename = "imageLookupBaseOS")]
    pub image_lookup_base_os: Option<String>, // `json:"imageLookupBaseOS,omitempty"`

    /// InstanceType is the type of instance to create. Example: m4.xlarge
    // +kubebuilder:validation:Required
    // +kubebuilder:validation:MinLength:=2
    pub instance_type: String, // `json:"instanceType"`

    /// AdditionalTags is an optional set of tags to add to an instance, in addition to the ones added by default by the
    /// AWS provider. If both the AWSCluster and the AWSMachine specify the same tag name with different values, the
    /// AWSMachine's value takes precedence.
    // +optional
    pub additional_tags: Option<Tags>, // `json:"additionalTags,omitempty"`

    /// IAMInstanceProfile is a name of an IAM instance profile to assign to the instance
    // +optional
    pub iam_instance_profile: Option<String>, // `json:"iamInstanceProfile,omitempty"`

    /// PublicIP specifies whether the instance should get a public IP.
    /// Precedence for this setting is as follows:
    /// 1. This field if set
    /// 2. Cluster/flavor setting
    /// 3. Subnet default
    // +optional
    #[serde(rename = "publicIP")]
    pub public_ip: Option<bool>, // `json:"publicIP,omitempty"`

    /// AdditionalSecurityGroups is an array of references to security groups that should be applied to the
    /// instance. These security groups would be set in addition to any security groups defined
    /// at the cluster level or in the actuator. It is possible to specify either IDs of Filters. Using Filters
    /// will cause additional requests to AWS API and if tags change the attached security groups might change too.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub additional_security_groups: Vec<types::AWSResourceReference>, //`json:"additionalSecurityGroups,omitempty"`

    // FailureDomain is the failure domain unique identifier this Machine should be attached to, as defined in Cluster API.
    // For this infrastructure provider, the ID is equivalent to an AWS Availability Zone.
    // If multiple subnets are matched for the availability zone, the first one returned is picked.
    pub failure_domain: Option<String>, //`json:"failureDomain,omitempty"`

    /// Subnet is a reference to the subnet to use for this instance. If not specified,
    /// the cluster subnet will be used.
    // +optional
    pub subnet: Option<types::AWSResourceReference>, //`json:"subnet,omitempty"`

    /// SSHKeyName is the name of the ssh key to attach to the instance. Valid values are empty string (do not use SSH keys), a valid SSH key name, or omitted (use the default SSH key name)
    // +optional
    pub ssh_key_name: Option<String>, //`json:"sshKeyName,omitempty"`

    /// RootVolume encapsulates the configuration options for the root volume
    // +optional
    pub root_volume: Option<Volume>, //`json:"rootVolume,omitempty"`

    /// Configuration options for the non root storage volumes.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub non_root_volumes: Vec<Volume>, //`json:"nonRootVolumes,omitempty"`

    /// NetworkInterfaces is a list of ENIs to associate with the instance.
    /// A maximum of 2 may be specified.
    // +optional
    // +kubebuilder:validation:MaxItems=2
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub network_interfaces: Vec<String>, //`json:"networkInterfaces,omitempty"`

    /// UncompressedUserData specify whether the user data is gzip-compressed before it is sent to ec2 instance.
    /// cloud-init has built-in support for gzip-compressed user data
    /// user data stored in aws secret manager is always gzip-compressed.
    // +optional
    pub uncompressed_user_data: Option<bool>, //`json:"uncompressedUserData,omitempty"`

    /// CloudInit defines options related to the bootstrapping systems where
    /// CloudInit is used.
    // +optional
    // pub cloud_init: Option<CloudInit>, //`json:"cloudInit,omitempty"`

    // /// SpotMarketOptions allows users to configure instances to be run using AWS Spot instances.
    // // +optional
    // pub spot_market_options: Option<SpotMarketOptions> , //`json:"spotMarketOptions,omitempty"`
    /// Tenancy indicates if instance should run on shared or single-tenant hardware.
    // +optional
    // +kubebuilder:validation:Enum:=default;dedicated;host
    pub tenancy: Option<String>, //`json:"tenancy,omitempty"`
}

/// AWSMachineStatus defines the observed state of AWSMachine.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AWSMachineStatus {
    /// Ready is true when the provider resource is ready.
    // +optional
    pub ready: Option<bool>, // `json:"ready"`

    /// Interruptible reports that this machine is using spot instances and can therefore be interrupted by CAPI when it receives a notice that the spot instance is to be terminated by AWS.
    /// This will be set to true when SpotMarketOptions is not nil (i.e. this machine is using a spot instance).
    // +optional
    pub interruptible: Option<bool>, // `json:"interruptible,omitempty"`

    /// Addresses contains the AWS instance associated addresses.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub addresses: Vec<clusterv1::MachineAddress>, // `json:"addresses,omitempty"`

    /// InstanceState is the state of the AWS instance for this machine.
    // +optional
    pub instance_state: Option<InstanceState>, // `json:"instanceState,omitempty"`

    /// FailureReason will be set in the event that there is a terminal problem
    /// reconciling the Machine and will contain a succinct value suitable
    /// for machine interpretation.
    ///
    /// This field should not be set for transitive errors that a controller
    /// faces that are expected to be fixed automatically over
    /// time (like service outages), but instead indicate that something is
    /// fundamentally wrong with the Machine's spec or the configuration of
    /// the controller, and that manual intervention is required. Examples
    /// of terminal errors would be invalid combinations of settings in the
    /// spec, values that are unsupported by the controller, or the
    /// responsible controller itself being critically misconfigured.
    ///
    /// Any transient errors that occur during the reconciliation of Machines
    /// can be added as events to the Machine object and/or logged in the
    /// controller's output.
    // +optional
    pub failure_reason: Option<errors::MachineStatusError>, // `json:"failureReason,omitempty"`

    /// FailureMessage will be set in the event that there is a terminal problem
    /// reconciling the Machine and will contain a more verbose string suitable
    /// for logging and human consumption.
    ///
    /// This field should not be set for transitive errors that a controller
    /// faces that are expected to be fixed automatically over
    /// time (like service outages), but instead indicate that something is
    /// fundamentally wrong with the Machine's spec or the configuration of
    /// the controller, and that manual intervention is required. Examples
    /// of terminal errors would be invalid combinations of settings in the
    /// spec, values that are unsupported by the controller, or the
    /// responsible controller itself being critically misconfigured.
    ///
    /// Any transient errors that occur during the reconciliation of Machines
    /// can be added as events to the Machine object and/or logged in the
    /// controller's output.
    // +optional
    pub failure_message: Option<String>, // `json:"failureMessage,omitempty"`

    /// Conditions defines current service state of the AWSMachine.
    // +optional
    pub conditions: Option<clusterv1::Conditions>, // `json:"conditions,omitempty"`
}

// package v1beta1

// import (
// 	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
// 	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
// 	"sigs.k8s.io/cluster-api/errors"
// )

// const (
// 	// MachineFinalizer allows ReconcileAWSMachine to clean up AWS resources associated with AWSMachine before
// 	// removing it from the apiserver.
// 	MachineFinalizer = "awsmachine.infrastructure.cluster.x-k8s.io"
// )

// // SecretBackend defines variants for backend secret storage.
// type SecretBackend string

// var (
// 	// SecretBackendSSMParameterStore defines AWS Systems Manager Parameter Store as the secret backend.
// 	SecretBackendSSMParameterStore = SecretBackend("ssm-parameter-store")

// 	// SecretBackendSecretsManager defines AWS Secrets Manager as the secret backend.
// 	SecretBackendSecretsManager = SecretBackend("secrets-manager")
// )

// // CloudInit defines options related to the bootstrapping systems where
// // CloudInit is used.
// type CloudInit struct {
// 	// InsecureSkipSecretsManager, when set to true will not use AWS Secrets Manager
// 	// or AWS Systems Manager Parameter Store to ensure privacy of userdata.
// 	// By default, a cloud-init boothook shell script is prepended to download
// 	// the userdata from Secrets Manager and additionally delete the secret.
// 	InsecureSkipSecretsManager bool `json:"insecureSkipSecretsManager,omitempty"`

// 	// SecretCount is the number of secrets used to form the complete secret
// 	// +optional
// 	SecretCount int32 `json:"secretCount,omitempty"`

// 	// SecretPrefix is the prefix for the secret name. This is stored
// 	// temporarily, and deleted when the machine registers as a node against
// 	// the workload cluster.
// 	// +optional
// 	SecretPrefix string `json:"secretPrefix,omitempty"`

// 	// SecureSecretsBackend, when set to parameter-store will utilize the AWS Systems Manager
// 	// Parameter Storage to distribute secrets. By default or with the value of secrets-manager,
// 	// will use AWS Secrets Manager instead.
// 	// +optional
// 	// +kubebuilder:validation:Enum=secrets-manager;ssm-parameter-store
// 	SecureSecretsBackend SecretBackend `json:"secureSecretsBackend,omitempty"`
// }

// // +kubebuilder:object:root=true
// // +kubebuilder:resource:path=awsmachines,scope=Namespaced,categories=cluster-api,shortName=awsm
// // +kubebuilder:storageversion
// // +kubebuilder:subresource:status
// // +kubebuilder:printcolumn:name="Cluster",type="string",JSONPath=".metadata.labels.cluster\\.x-k8s\\.io/cluster-name",description="Cluster to which this AWSMachine belongs"
// // +kubebuilder:printcolumn:name="State",type="string",JSONPath=".status.instanceState",description="EC2 instance state"
// // +kubebuilder:printcolumn:name="Ready",type="string",JSONPath=".status.ready",description="Machine ready status"
// // +kubebuilder:printcolumn:name="InstanceID",type="string",JSONPath=".spec.providerID",description="EC2 instance ID"
// // +kubebuilder:printcolumn:name="Machine",type="string",JSONPath=".metadata.ownerReferences[?(@.kind==\"Machine\")].name",description="Machine object which owns with this AWSMachine"
// // +k8s:defaulter-gen=true

// // AWSMachine is the schema for Amazon EC2 machines.
// type AWSMachine struct {
// 	metav1.TypeMeta   `json:",inline"`
// 	metav1.ObjectMeta `json:"metadata,omitempty"`

// 	Spec   AWSMachineSpec   `json:"spec,omitempty"`
// 	Status AWSMachineStatus `json:"status,omitempty"`
// }

// // GetConditions returns the observations of the operational state of the AWSMachine resource.
// func (r *AWSMachine) GetConditions() clusterv1.Conditions {
// 	return r.Status.Conditions
// }

// // SetConditions sets the underlying service state of the AWSMachine to the predescribed clusterv1.Conditions.
// func (r *AWSMachine) SetConditions(conditions clusterv1.Conditions) {
// 	r.Status.Conditions = conditions
// }

// // +kubebuilder:object:root=true

// // AWSMachineList contains a list of Amazon EC2 machines.
// type AWSMachineList struct {
// 	metav1.TypeMeta `json:",inline"`
// 	metav1.ListMeta `json:"metadata,omitempty"`
// 	Items           []AWSMachine `json:"items"`
// }

// func init() {
// 	SchemeBuilder.Register(&AWSMachine{}, &AWSMachineList{})
// }

/*
package v1beta1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

// +kubebuilder:object:root=true
// +kubebuilder:resource:path=awsmachinetemplates,scope=Namespaced,categories=cluster-api,shortName=awsmt
// +kubebuilder:storageversion
// +k8s:defaulter-gen=true

// AWSMachineTemplate is the schema for the Amazon EC2 Machine Templates API.
type AWSMachineTemplate struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec AWSMachineTemplateSpec `json:"spec,omitempty"`
}

// +kubebuilder:object:root=true

// AWSMachineTemplateList contains a list of AWSMachineTemplate.
type AWSMachineTemplateList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []AWSMachineTemplate `json:"items"`
}

func init() {
    SchemeBuilder.Register(&AWSMachineTemplate{}, &AWSMachineTemplateList{})
}
*/
