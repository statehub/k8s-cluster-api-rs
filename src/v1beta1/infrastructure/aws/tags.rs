use std::convert::AsRef;

use super::*;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Tags(BTreeMap<String, String>);

impl Tags {
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
}

impl AsRef<BTreeMap<String, String>> for Tags {
    fn as_ref(&self) -> &BTreeMap<String, String> {
        &self.0
    }
}
