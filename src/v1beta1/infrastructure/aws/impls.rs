use super::*;

impl AWSClusterSpec {
    pub fn region(&self) -> Option<&str> {
        self.region.as_deref()
    }

    pub fn network(&self) -> Option<&NetworkSpec> {
        self.network.as_ref()
    }

    pub fn vpc(&self) -> Option<&VPCSpec> {
        self.network().and_then(|network| network.vpc.as_ref())
    }

    pub fn vpc_id(&self) -> Option<&str> {
        self.vpc().and_then(|vpc| vpc.id.as_deref())
    }
}

impl AWSClusterStatus {
    pub fn ready(&self) -> bool {
        self.ready
    }
    pub fn network(&self) -> Option<&NetworkStatus> {
        self.network_status.as_ref()
    }
}

impl AWSCluster {
    pub fn with_name(name: &str) -> Self {
        let spec = AWSClusterSpec::default();
        Self::new(name, spec)
    }

    #[must_use]
    pub fn region(mut self, region: impl ToString) -> Self {
        self.spec.region = Some(region.to_string());
        self
    }

    #[must_use]
    pub fn sshkey(mut self, sshkey: impl ToString) -> Self {
        self.spec.ssh_key_name = Some(sshkey.to_string());
        self
    }

    #[must_use]
    pub fn vpc_cidr_block(mut self, cidr_block: impl ToString) -> Self {
        let cidr_block = cidr_block.to_string();
        let mut network = self.spec.network.unwrap_or_default();
        let mut vpc = network.vpc.unwrap_or_default();
        vpc.cidr_block = Some(cidr_block);
        network.vpc = Some(vpc);
        self.spec.network = Some(network);

        // let vpc_cidr_block = vpc_cidr_block.to_string();
        // let network = self.spec.network.unwrap_or_default();
        // let vpc = network.vpc.unwrap_or_default();
        // let vpc = VPCSpec {
        //     cidr_block: Some(vpc_cidr_block),
        //     ..vpc
        // };
        // let network = NetworkSpec {
        //     vpc: Some(vpc),
        //     ..network
        // };

        // Self {
        //     spec: AWSClusterSpec {
        //         network: Some(network),
        //         ..self.spec
        //     },
        //     ..self
        // }
        self
    }

    pub fn add_subnet(&mut self, availability_zone: &str, cidr_block: &str, is_public: bool) {
        let network = self.spec.network.take().unwrap_or_default();
        let network::Subnets(mut subnets) = network.subnets.unwrap_or_default();
        subnets.push(network::SubnetSpec {
            availability_zone: Some(availability_zone.to_string()),
            cidr_block: Some(cidr_block.to_string()),
            is_public: Some(is_public),
            ..Default::default()
        });
        let network = NetworkSpec {
            subnets: Some(network::Subnets(subnets)),
            ..network
        };
        self.spec.network = Some(network);
    }

    #[must_use]
    pub fn namespace(self, namespace: &str) -> Self {
        Self {
            metadata: kube::core::ObjectMeta {
                namespace: Some(namespace.to_string()),
                ..self.metadata
            },
            ..self
        }
    }

    pub fn bastion(self, enabled: bool) -> Self {
        Self {
            spec: AWSClusterSpec {
                bastion: Some(Bastion {
                    enabled: Some(enabled),
                    allowed_cidr_blocks: vec!["0.0.0.0/0".to_string()],
                    ..Bastion::default()
                }),
                ..self.spec
            },
            ..self
        }
    }
}
