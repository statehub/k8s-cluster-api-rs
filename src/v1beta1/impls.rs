use kube::Resource;

use super::*;

impl ControlPlaneConfigTemplate for KubeadmConfigTemplate {
    fn object_ref(&self) -> corev1::ObjectReference {
        kube::Resource::object_ref(self, &())
    }
}

impl ControlPlane {
    pub fn object_ref(&self) -> corev1::ObjectReference {
        match self {
            ControlPlane::Kubeadm(kubeadm) => kubeadm.object_ref(&()),
        }
    }
}

impl Infrastructure {
    pub fn object_ref(&self) -> corev1::ObjectReference {
        match self {
            Infrastructure::Aws(aws) => aws.object_ref(&()),
        }
    }
}

impl MachineTemplate for AWSMachineTemplate {
    fn object_ref(&self) -> corev1::ObjectReference {
        kube::Resource::object_ref(self, &())
    }
}
