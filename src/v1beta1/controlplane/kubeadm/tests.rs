use kube::ResourceExt;
use serde_json as json;

use super::*;

#[test]
fn de() {
    let object: KubeadmControlPlane = json::from_str(OBJECT).unwrap();

    assert_eq!(object.name(), "mgmt1-control-plane");

    let spec = object.spec;
    let status = object.status.unwrap();

    assert_eq!(spec.replicas, Some(3));
    assert_eq!(spec.version, "v1.21.6");
    assert_eq!(status.replicas, Some(3));
    assert_eq!(status.version.as_deref(), Some("v1.21.6"));
}

const OBJECT: &str = r#"
{
    "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
    "kind": "KubeadmControlPlane",
    "metadata": {
        "annotations": {
            "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"metadata\":{\"annotations\":{},\"name\":\"mgmt1-control-plane\",\"namespace\":\"default\"},\"spec\":{\"kubeadmConfigSpec\":{\"clusterConfiguration\":{\"apiServer\":{\"extraArgs\":{\"cloud-provider\":\"aws\"}},\"controllerManager\":{\"extraArgs\":{\"cloud-provider\":\"aws\"}}},\"initConfiguration\":{\"nodeRegistration\":{\"kubeletExtraArgs\":{\"cloud-provider\":\"aws\"},\"name\":\"{{ ds.meta_data.local_hostname }}\"}},\"joinConfiguration\":{\"nodeRegistration\":{\"kubeletExtraArgs\":{\"cloud-provider\":\"aws\"},\"name\":\"{{ ds.meta_data.local_hostname }}\"}}},\"machineTemplate\":{\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSMachineTemplate\",\"name\":\"mgmt1-control-plane\"}},\"replicas\":3,\"version\":\"v1.21.6\"}}\n"
        },
        "creationTimestamp": "2021-11-30T13:35:08Z",
        "finalizers": [
            "kubeadm.controlplane.cluster.x-k8s.io"
        ],
        "generation": 1,
        "labels": {
            "cluster.x-k8s.io/cluster-name": "mgmt1"
        },
        "name": "mgmt1-control-plane",
        "namespace": "default",
        "ownerReferences": [
            {
                "apiVersion": "cluster.x-k8s.io/v1beta1",
                "blockOwnerDeletion": true,
                "controller": true,
                "kind": "Cluster",
                "name": "mgmt1",
                "uid": "94f05757-dcc1-447c-b020-844301437928"
            }
        ],
        "resourceVersion": "10520579",
        "uid": "4b3b148b-ad25-484c-9adb-5007e8f28b72"
    },
    "spec": {
        "kubeadmConfigSpec": {
            "clusterConfiguration": {
                "apiServer": {
                    "extraArgs": {
                        "cloud-provider": "aws"
                    }
                },
                "controllerManager": {
                    "extraArgs": {
                        "cloud-provider": "aws"
                    }
                },
                "dns": {},
                "etcd": {},
                "networking": {},
                "scheduler": {}
            },
            "initConfiguration": {
                "localAPIEndpoint": {},
                "nodeRegistration": {
                    "kubeletExtraArgs": {
                        "cloud-provider": "aws"
                    },
                    "name": "{{ ds.meta_data.local_hostname }}"
                }
            },
            "joinConfiguration": {
                "discovery": {},
                "nodeRegistration": {
                    "kubeletExtraArgs": {
                        "cloud-provider": "aws"
                    },
                    "name": "{{ ds.meta_data.local_hostname }}"
                }
            }
        },
        "machineTemplate": {
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSMachineTemplate",
                "name": "mgmt1-control-plane",
                "namespace": "default"
            },
            "metadata": {}
        },
        "replicas": 3,
        "rolloutStrategy": {
            "rollingUpdate": {
                "maxSurge": 1
            },
            "type": "RollingUpdate"
        },
        "version": "v1.21.6"
    },
    "status": {
        "conditions": [
            {
                "lastTransitionTime": "2021-11-30T13:44:16Z",
                "status": "True",
                "type": "Ready"
            },
            {
                "lastTransitionTime": "2021-11-30T13:42:07Z",
                "status": "True",
                "type": "Available"
            },
            {
                "lastTransitionTime": "2021-11-30T13:40:05Z",
                "status": "True",
                "type": "CertificatesAvailable"
            },
            {
                "lastTransitionTime": "2021-12-08T00:11:30Z",
                "status": "True",
                "type": "ControlPlaneComponentsHealthy"
            },
            {
                "lastTransitionTime": "2021-12-08T00:03:30Z",
                "status": "True",
                "type": "EtcdClusterHealthy"
            },
            {
                "lastTransitionTime": "2021-11-30T13:43:50Z",
                "status": "True",
                "type": "MachinesCreated"
            },
            {
                "lastTransitionTime": "2021-11-30T13:44:16Z",
                "status": "True",
                "type": "MachinesReady"
            },
            {
                "lastTransitionTime": "2021-11-30T13:44:13Z",
                "status": "True",
                "type": "Resized"
            }
        ],
        "initialized": true,
        "observedGeneration": 1,
        "ready": true,
        "readyReplicas": 3,
        "replicas": 3,
        "selector": "cluster.x-k8s.io/cluster-name=mgmt1,cluster.x-k8s.io/control-plane",
        "unavailableReplicas": 0,
        "updatedReplicas": 3,
        "version": "v1.21.6"
    }
}
"#;
