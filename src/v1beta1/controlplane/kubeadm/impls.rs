use maplit::{btreemap, convert_args};

use super::*;

impl KubeadmControlPlane {
    pub fn aws(name: &str) -> Self {
        let extra_args = convert_args!(btreemap!("cloud-provider" => "aws"));
        let spec = KubeadmControlPlaneSpec {
            kubeadm_config_spec: cabpkv1::KubeadmConfigSpec {
                cluster_configuration: Some(cabpkv1::ClusterConfiguration {
                    api_server: Some(cabpkv1::APIServer {
                        control_plane: cabpkv1::ControlPlaneComponent {
                            extra_args: extra_args.clone(),
                            ..Default::default()
                        },
                        ..Default::default()
                    }),
                    controller_manager: Some(cabpkv1::ControlPlaneComponent {
                        extra_args: extra_args.clone(),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                init_configuration: Some(cabpkv1::InitConfiguration {
                    node_registration: Some(cabpkv1::NodeRegistrationOptions {
                        kubelet_extra_args: extra_args.clone(),
                        name: Some("{{ ds.meta_data.local_hostname }}".to_string()),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                join_configuration: Some(cabpkv1::JoinConfiguration {
                    node_registration: Some(cabpkv1::NodeRegistrationOptions {
                        kubelet_extra_args: extra_args,
                        name: Some("{{ ds.meta_data.local_hostname }}".to_string()),
                        ..Default::default()
                    }),
                    ..Default::default()
                }),
                ..Default::default()
            },
            ..Default::default()
        };
        Self::new(name, spec)
    }

    #[must_use]
    pub fn machine_template<T>(self, machine_template: &T) -> Self
    where
        T: MachineTemplate,
    {
        let infrastructure_ref = machine_template.object_ref();
        let mut kubeadm_control_plane_machine_template = self.spec.machine_template;
        kubeadm_control_plane_machine_template.infrastructure_ref = infrastructure_ref;
        Self {
            spec: KubeadmControlPlaneSpec {
                machine_template: kubeadm_control_plane_machine_template,
                ..self.spec
            },
            ..self
        }
    }

    #[must_use]
    pub fn replicas(self, replicas: i32) -> Self {
        let mut kubeadm_control_plane_spec = self.spec;
        kubeadm_control_plane_spec.replicas = Some(replicas);
        Self {
            spec: kubeadm_control_plane_spec,
            ..self
        }
    }

    #[must_use]
    pub fn version(self, version: &str) -> Self {
        Self {
            spec: KubeadmControlPlaneSpec {
                version: version.to_string(),
                ..self.spec
            },
            ..self
        }
    }

    #[must_use]
    pub fn namespace(self, namespace: &str) -> Self {
        Self {
            metadata: kube::core::ObjectMeta {
                namespace: Some(namespace.to_string()),
                ..self.metadata
            },
            ..self
        }
    }
}
