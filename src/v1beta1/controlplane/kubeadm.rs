use super::*;

mod impls;

/// KubeadmControlPlaneSpec defines the desired state of KubeadmControlPlane.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize, CustomResource)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "controlplane.cluster.x-k8s.io",
    version = "v1beta1",
    kind = "KubeadmControlPlane",
    plural = "kubeadmcontrolplanes",
    shortname = "kcp",
    status = "KubeadmControlPlaneStatus"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct KubeadmControlPlaneSpec {
    /// Number of desired machines. Defaults to 1. When stacked etcd is used only
    /// odd numbers are permitted, as per [etcd best practice](https://etcd.io/docs/v3.3.12/faq/#why-an-odd-number-of-cluster-members).
    /// This is a pointer to distinguish between explicit zero and not specified.
    // +optional
    pub replicas: Option<i32>, // `json:"replicas,omitempty"`

    /// Version defines the desired Kubernetes version.
    pub version: String, // `json:"version"`

    /// MachineTemplate contains information about how machines
    /// should be shaped when creating or updating a control plane.
    pub machine_template: KubeadmControlPlaneMachineTemplate, // `json:"machineTemplate"`

    /// KubeadmConfigSpec is a KubeadmConfigSpec
    /// to use for initializing and joining machines to the control plane.
    pub kubeadm_config_spec: cabpkv1::KubeadmConfigSpec, //`json:"kubeadmConfigSpec"`

    /// RolloutAfter is a field to indicate a rollout should be performed
    /// after the specified time even if no changes have been made to the
    /// KubeadmControlPlane.
    //
    // +optional
    pub rollout_after: Option<metav1::Time>, // `json:"rolloutAfter,omitempty"`

    /// The RolloutStrategy to use to replace control plane machines with
    /// new ones.
    // +optional
    // +kubebuilder:default={type: "RollingUpdate", rollingUpdate: {maxSurge: 1}}
    pub rollout_strategy: Option<RolloutStrategy>, // `json:"rolloutStrategy,omitempty"`
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
/// KubeadmControlPlaneStatus defines the observed state of KubeadmControlPlane.
pub struct KubeadmControlPlaneStatus {
    // Selector is the label selector in string format to avoid introspection
    // by clients, and is used to provide the CRD-based integration for the
    // scale subresource and additional integrations for things like kubectl
    // describe.. The string will be in the same format as the query-param syntax.
    // More info about label selectors: http://kubernetes.io/docs/user-guide/labels#label-selectors
    // +optional
    pub selector: Option<String>, // `json:"selector,omitempty"`

    // Total number of non-terminated machines targeted by this control plane
    // (their labels match the selector).
    // +optional
    pub replicas: Option<i32>, // `json:"replicas"`

    // Version represents the minimum Kubernetes version for the control plane machines
    // in the cluster.
    // +optional
    pub version: Option<String>, // `json:"version,omitempty"`

    // Total number of non-terminated machines targeted by this control plane
    // that have the desired template spec.
    // +optional
    pub updated_replicas: Option<i32>, // `json:"updatedReplicas"`

    // Total number of fully running and ready control plane machines.
    // +optional
    pub ready_replicas: Option<i32>, // `json:"readyReplicas"`

    // Total number of unavailable machines targeted by this control plane.
    // This is the total number of machines that are still required for
    // the deployment to have 100% available capacity. They may either
    // be machines that are running but not yet ready or machines
    // that still have not been created.
    // +optional
    pub unavailable_replicas: Option<i32>, // `json:"unavailableReplicas"`

    // Initialized denotes whether or not the control plane has the
    // uploaded kubeadm-config configmap.
    // +optional
    pub initialized: Option<bool>, // `json:"initialized"`

    // Ready denotes that the KubeadmControlPlane API Server is ready to
    // receive requests.
    // +optional
    pub ready: Option<bool>, // `json:"ready"`

    // FailureReason indicates that there is a terminal problem reconciling the
    // state, and will be set to a token value suitable for
    // programmatic interpretation.
    // +optional
    pub failure_reason: Option<errors::KubeadmControlPlaneStatusError>, // `json:"failureReason,omitempty"`

    // ErrorMessage indicates that there is a terminal problem reconciling the
    // state, and will be set to a descriptive error message.
    // +optional
    pub failure_message: Option<String>, // `json:"failureMessage,omitempty"`

    // ObservedGeneration is the latest generation observed by the controller.
    // +optional
    pub observed_generation: Option<i64>, // `json:"observedGeneration,omitempty"`

    // Conditions defines current service state of the KubeadmControlPlane.
    // +optional
    pub conditions: Option<clusterv1::Conditions>, // `json:"conditions,omitempty"`
}

/// KubeadmControlPlaneMachineTemplate defines the template for Machines
/// in a KubeadmControlPlane object.
#[serde_as]
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct KubeadmControlPlaneMachineTemplate {
    /// Standard object's metadata.
    /// More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
    // +optional
    pub metadata: Option<clusterv1::ObjectMeta>, // `json:"metadata,omitempty"`

    /// InfrastructureRef is a required reference to a custom resource
    /// offered by an infrastructure provider.
    pub infrastructure_ref: corev1::ObjectReference, // `json:"infrastructureRef"`

    /// NodeDrainTimeout is the total amount of time that the controller will spend on draining a controlplane node
    /// The default value is 0, meaning that the node can be drained without any time limitations.
    /// NOTE: NodeDrainTimeout is different from `kubectl drain --timeout`
    // +optional
    // #[serde_as(as = "serde_with::DurationSeconds<i64>")]
    // pub node_drain_timeout: Option<chrono::Duration>, // `json:"nodeDrainTimeout,omitempty"`
    pub node_drain_timeout: Option<i64>, // `json:"nodeDrainTimeout,omitempty"`
}

/// RolloutStrategy describes how to replace existing machines
/// with new ones.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RolloutStrategy {
    /// Type of rollout. Currently the only supported strategy is
    /// "RollingUpdate".
    /// Default is RollingUpdate.
    // +optional
    pub r#type: Option<RolloutStrategyType>, // `json:"type,omitempty"`

    /// Rolling update config params. Present only if
    /// RolloutStrategyType = RollingUpdate.
    // +optional
    pub rolling_update: Option<RollingUpdate>, // `json:"rollingUpdate,omitempty"`
}

/// RolloutStrategyType defines the rollout strategies for a KubeadmControlPlane.
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum RolloutStrategyType {
    /// RollingUpdateStrategyType replaces the old control planes by new one using rolling update
    /// i.e. gradually scale up or down the old control planes and scale up or down the new one.
    RollingUpdate,
}

/// RollingUpdate is used to control the desired behavior of rolling update.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RollingUpdate {
    /// The maximum number of control planes that can be scheduled above or under the
    /// desired number of control planes.
    /// Value can be an absolute number 1 or 0.
    /// Defaults to 1.
    /// Example: when this is set to 1, the control plane can be scaled
    /// up immediately when the rolling update starts.
    // +optional
    pub max_surge: Option<intstr::IntOrString>, // `json:"maxSurge,omitempty"`
}

#[cfg(test)]
mod tests;

/* ======

package v1beta1

import (
    corev1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/util/intstr"
    clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
    cabpkv1 "sigs.k8s.io/cluster-api/bootstrap/kubeadm/api/v1beta1"
    "sigs.k8s.io/cluster-api/errors"
)


const (
    // RollingUpdateStrategyType replaces the old control planes by new one using rolling update
    // i.e. gradually scale up or down the old control planes and scale up or down the new one.
    RollingUpdateStrategyType RolloutStrategyType = "RollingUpdate"
)

const (
    // KubeadmControlPlaneFinalizer is the finalizer applied to KubeadmControlPlane resources
    // by its managing controller.
    KubeadmControlPlaneFinalizer = "kubeadm.controlplane.cluster.x-k8s.io"

    // SkipCoreDNSAnnotation annotation explicitly skips reconciling CoreDNS if set.
    SkipCoreDNSAnnotation = "controlplane.cluster.x-k8s.io/skip-coredns"

    // SkipKubeProxyAnnotation annotation explicitly skips reconciling kube-proxy if set.
    SkipKubeProxyAnnotation = "controlplane.cluster.x-k8s.io/skip-kube-proxy"

    // KubeadmClusterConfigurationAnnotation is a machine annotation that stores the json-marshalled string of KCP ClusterConfiguration.
    // This annotation is used to detect any changes in ClusterConfiguration and trigger machine rollout in KCP.
    KubeadmClusterConfigurationAnnotation = "controlplane.cluster.x-k8s.io/kubeadm-cluster-configuration"
)


// +kubebuilder:object:root=true
// +kubebuilder:resource:path=kubeadmcontrolplanes,shortName=kcp,scope=Namespaced,categories=cluster-api
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:printcolumn:name="Cluster",type="string",JSONPath=".metadata.labels['cluster\\.x-k8s\\.io/cluster-name']",description="Cluster"
// +kubebuilder:printcolumn:name="Initialized",type=boolean,JSONPath=".status.initialized",description="This denotes whether or not the control plane has the uploaded kubeadm-config configmap"
// +kubebuilder:printcolumn:name="API Server Available",type=boolean,JSONPath=".status.ready",description="KubeadmControlPlane API Server is ready to receive requests"
// +kubebuilder:printcolumn:name="Replicas",type=integer,JSONPath=".status.replicas",description="Total number of non-terminated machines targeted by this control plane"
// +kubebuilder:printcolumn:name="Ready",type=integer,JSONPath=".status.readyReplicas",description="Total number of fully running and ready control plane machines"
// +kubebuilder:printcolumn:name="Updated",type=integer,JSONPath=".status.updatedReplicas",description="Total number of non-terminated machines targeted by this control plane that have the desired template spec"
// +kubebuilder:printcolumn:name="Unavailable",type=integer,JSONPath=".status.unavailableReplicas",description="Total number of unavailable machines targeted by this control plane"
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Time duration since creation of KubeadmControlPlane"
// +kubebuilder:printcolumn:name="Version",type=string,JSONPath=".spec.version",description="Kubernetes version associated with this control plane"

// KubeadmControlPlane is the Schema for the KubeadmControlPlane API.
type KubeadmControlPlane struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec   KubeadmControlPlaneSpec   `json:"spec,omitempty"`
    Status KubeadmControlPlaneStatus `json:"status,omitempty"`
}

// GetConditions returns the set of conditions for this object.
func (in *KubeadmControlPlane) GetConditions() clusterv1.Conditions {
    return in.Status.Conditions
}

// SetConditions sets the conditions on this object.
func (in *KubeadmControlPlane) SetConditions(conditions clusterv1.Conditions) {
    in.Status.Conditions = conditions
}

// +kubebuilder:object:root=true

// KubeadmControlPlaneList contains a list of KubeadmControlPlane.
type KubeadmControlPlaneList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []KubeadmControlPlane `json:"items"`
}

func init() {
    SchemeBuilder.Register(&KubeadmControlPlane{}, &KubeadmControlPlaneList{})
}


======= */
