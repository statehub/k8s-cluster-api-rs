use std::fmt;

use super::*;

/// ClusterPhase is a string representation of a Cluster Phase.
///
/// This type is a high-level indicator of the status of the Cluster as it is provisioned,
/// from the API user’s perspective.
///
/// The value should not be interpreted by any software components as a reliable indication
/// of the actual state of the Cluster, and controllers should not use the Cluster Phase field
/// value when making decisions about what action to take.
///
/// Controllers should always look at the actual state of the Cluster’s fields to make those decisions.
///

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum ClusterPhase {
    /// ClusterPhasePending is the first state a Cluster is assigned by
    /// Cluster API Cluster controller after being created.
    Pending,

    /// Provisioning is the state when the Cluster has a provider infrastructure
    /// object associated and can start provisioning.
    Provisioning,

    /// Provisioned is the state when its
    /// infrastructure has been created and configured.
    Provisioned,

    /// Deleting is the Cluster state when a delete
    /// request has been sent to the API Server,
    /// but its infrastructure has not yet been fully deleted.
    Deleting,

    /// Failed is the Cluster state when the system
    /// might require user intervention.
    Failed,

    /// Unknown is returned if the Cluster state cannot be determined.
    Unknown,
}

impl Default for ClusterPhase {
    fn default() -> Self {
        Self::Unknown
    }
}

impl ClusterPhase {
    pub fn to_str(&self) -> &'static str {
        match self {
            Self::Pending => "Pending",
            Self::Provisioning => "Provisioning",
            Self::Provisioned => "Provisioned",
            Self::Deleting => "Deleting",
            Self::Failed => "Failed",
            Self::Unknown => "Unknown",
        }
    }
}

impl fmt::Display for ClusterPhase {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.to_str().fmt(f)
    }
}
