use super::*;

use kube::{core::object::HasStatus, ResourceExt};
use serde_json as json;

#[test]
fn de() {
    let clusters: Vec<Cluster> = json::from_str(CLUSTER).unwrap();
    assert_eq!(clusters.len(), 6);

    let mgmt1 = clusters.get(0).unwrap();

    assert_eq!(mgmt1.name(), "mgmt1");
    assert!(mgmt1.status().is_some());
    assert_eq!(mgmt1.phase(), ClusterPhase::Provisioned);
    assert_eq!(
        mgmt1.get_ip_family().unwrap(),
        ClusterIpFamily::Ipv4IpFamily,
    );
}

const CLUSTER: &str = r#"
[
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"mgmt1\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.0.0/16\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"mgmt1-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"mgmt1\"}}}\n"
            },
            "creationTimestamp": "2021-11-30T13:35:07Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 2,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-30T13:35:07Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:spec": {
                            "f:controlPlaneEndpoint": {
                                "f:host": {},
                                "f:port": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:controlPlaneReady": {},
                            "f:failureDomains": {
                                ".": {},
                                "f:us-east-2a": {
                                    ".": {},
                                    "f:controlPlane": {}
                                },
                                "f:us-east-2b": {
                                    ".": {},
                                    "f:controlPlane": {}
                                }
                            },
                            "f:infrastructureReady": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-30T13:42:55Z"
                }
            ],
            "name": "mgmt1",
            "namespace": "default",
            "resourceVersion": "6018659",
            "uid": "94f05757-dcc1-447c-b020-844301437928"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.0.0/16"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "mgmt1-apiserver-1759940877.us-east-2.elb.amazonaws.com",
                "port": 6443
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "mgmt1-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "mgmt1",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-30T13:44:16Z",
                    "status": "True",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:42:07Z",
                    "status": "True",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:44:16Z",
                    "status": "True",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:40:03Z",
                    "status": "True",
                    "type": "InfrastructureReady"
                }
            ],
            "controlPlaneReady": true,
            "failureDomains": {
                "us-east-2a": {
                    "controlPlane": true
                },
                "us-east-2b": {
                    "controlPlane": true
                }
            },
            "infrastructureReady": true,
            "observedGeneration": 2,
            "phase": "Provisioned"
        }
    },
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"pop-1\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.200.0/24\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"pop-1-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"pop-1\"}}}\n"
            },
            "creationTimestamp": "2021-11-30T13:38:02Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 2,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-30T13:38:02Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:spec": {
                            "f:controlPlaneEndpoint": {
                                "f:host": {},
                                "f:port": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:controlPlaneReady": {},
                            "f:failureDomains": {
                                ".": {},
                                "f:us-east-1a": {
                                    ".": {},
                                    "f:controlPlane": {}
                                },
                                "f:us-east-1b": {
                                    ".": {},
                                    "f:controlPlane": {}
                                }
                            },
                            "f:infrastructureReady": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-30T13:55:16Z"
                }
            ],
            "name": "pop-1",
            "namespace": "default",
            "resourceVersion": "6023387",
            "uid": "6367f162-46d5-4dd6-9f31-1acf16bd6160"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.200.0/24"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "pop-1-apiserver-1435843608.us-east-1.elb.amazonaws.com",
                "port": 6443
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "pop-1-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "pop-1",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-30T13:55:12Z",
                    "status": "True",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:52:42Z",
                    "status": "True",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:55:12Z",
                    "status": "True",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-30T13:50:35Z",
                    "status": "True",
                    "type": "InfrastructureReady"
                }
            ],
            "controlPlaneReady": true,
            "failureDomains": {
                "us-east-1a": {
                    "controlPlane": true
                },
                "us-east-1b": {
                    "controlPlane": true
                }
            },
            "infrastructureReady": true,
            "observedGeneration": 2,
            "phase": "Provisioned"
        }
    },
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"pop-10\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.200.0/24\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"pop-10-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"pop-10\"}}}\n"
            },
            "creationTimestamp": "2021-11-23T15:30:11Z",
            "deletionGracePeriodSeconds": 0,
            "deletionTimestamp": "2021-11-23T15:35:37Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 2,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-23T15:30:11Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-23T15:30:41Z"
                }
            ],
            "name": "pop-10",
            "namespace": "default",
            "resourceVersion": "2300360",
            "uid": "a085cd8a-285d-4cf0-bf2b-59a4f9492fb1"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.200.0/24"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "",
                "port": 0
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "pop-10-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "pop-10",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-23T15:30:41Z",
                    "message": "0 of 7 completed",
                    "reason": "VpcReconciliationFailed",
                    "severity": "Error",
                    "status": "False",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-23T15:30:41Z",
                    "message": "Waiting for control plane provider to indicate the control plane has been initialized",
                    "reason": "WaitingForControlPlaneProviderInitialized",
                    "severity": "Info",
                    "status": "False",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-23T15:35:42Z",
                    "reason": "Deleted",
                    "severity": "Info",
                    "status": "False",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-23T15:30:41Z",
                    "message": "0 of 7 completed",
                    "reason": "VpcReconciliationFailed",
                    "severity": "Error",
                    "status": "False",
                    "type": "InfrastructureReady"
                }
            ],
            "observedGeneration": 2,
            "phase": "Deleting"
        }
    },
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"pop-12\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.200.0/24\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"pop-12-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"pop-12\"}}}\n"
            },
            "creationTimestamp": "2021-11-25T15:42:15Z",
            "deletionGracePeriodSeconds": 0,
            "deletionTimestamp": "2021-11-25T16:07:25Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 3,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-25T15:42:15Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:spec": {
                            "f:controlPlaneEndpoint": {
                                "f:host": {},
                                "f:port": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:controlPlaneReady": {},
                            "f:failureDomains": {
                                ".": {},
                                "f:us-east-1a": {
                                    ".": {},
                                    "f:controlPlane": {}
                                },
                                "f:us-east-1b": {
                                    ".": {},
                                    "f:controlPlane": {}
                                }
                            },
                            "f:infrastructureReady": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-25T15:49:25Z"
                }
            ],
            "name": "pop-12",
            "namespace": "default",
            "resourceVersion": "3296686",
            "uid": "3d1c0c8f-8fe5-4044-9635-3fdb103acb5c"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.200.0/24"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "pop-12-apiserver-996970060.us-east-1.elb.amazonaws.com",
                "port": 6443
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "pop-12-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "pop-12",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-25T15:51:26Z",
                    "status": "True",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-25T15:48:45Z",
                    "status": "True",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-25T15:51:26Z",
                    "status": "True",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-25T15:47:00Z",
                    "status": "True",
                    "type": "InfrastructureReady"
                }
            ],
            "controlPlaneReady": true,
            "failureDomains": {
                "us-east-1a": {
                    "controlPlane": true
                },
                "us-east-1b": {
                    "controlPlane": true
                }
            },
            "infrastructureReady": true,
            "observedGeneration": 3,
            "phase": "Deleting"
        }
    },
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"pop-14\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.200.0/24\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"pop-14-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"pop-14\"}}}\n"
            },
            "creationTimestamp": "2021-11-25T16:18:20Z",
            "deletionGracePeriodSeconds": 0,
            "deletionTimestamp": "2021-12-01T07:54:12Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 3,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-25T16:18:20Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:spec": {
                            "f:controlPlaneEndpoint": {
                                "f:host": {},
                                "f:port": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:controlPlaneReady": {},
                            "f:failureDomains": {
                                ".": {},
                                "f:us-east-1a": {
                                    ".": {},
                                    "f:controlPlane": {}
                                },
                                "f:us-east-1b": {
                                    ".": {},
                                    "f:controlPlane": {}
                                }
                            },
                            "f:infrastructureReady": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-25T16:29:00Z"
                }
            ],
            "name": "pop-14",
            "namespace": "default",
            "resourceVersion": "6438824",
            "uid": "9aab766b-e553-478f-96bb-b6cc853c482f"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.200.0/24"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "pop-14-apiserver-102244197.us-east-1.elb.amazonaws.com",
                "port": 6443
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "pop-14-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "pop-14",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-25T16:30:47Z",
                    "status": "True",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-25T16:28:29Z",
                    "status": "True",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-25T16:30:47Z",
                    "status": "True",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-25T16:26:29Z",
                    "status": "True",
                    "type": "InfrastructureReady"
                }
            ],
            "controlPlaneReady": true,
            "failureDomains": {
                "us-east-1a": {
                    "controlPlane": true
                },
                "us-east-1b": {
                    "controlPlane": true
                }
            },
            "infrastructureReady": true,
            "observedGeneration": 3,
            "phase": "Deleting"
        }
    },
    {
        "apiVersion": "cluster.x-k8s.io/v1beta1",
        "kind": "Cluster",
        "metadata": {
            "annotations": {
                "kubectl.kubernetes.io/last-applied-configuration": "{\"apiVersion\":\"cluster.x-k8s.io/v1beta1\",\"kind\":\"Cluster\",\"metadata\":{\"annotations\":{},\"name\":\"pop-9\",\"namespace\":\"default\"},\"spec\":{\"clusterNetwork\":{\"pods\":{\"cidrBlocks\":[\"192.168.200.0/24\"]}},\"controlPlaneRef\":{\"apiVersion\":\"controlplane.cluster.x-k8s.io/v1beta1\",\"kind\":\"KubeadmControlPlane\",\"name\":\"pop-9-control-plane\"},\"infrastructureRef\":{\"apiVersion\":\"infrastructure.cluster.x-k8s.io/v1beta1\",\"kind\":\"AWSCluster\",\"name\":\"pop-9\"}}}\n"
            },
            "creationTimestamp": "2021-11-23T14:45:31Z",
            "deletionGracePeriodSeconds": 0,
            "deletionTimestamp": "2021-11-23T15:28:54Z",
            "finalizers": [
                "cluster.cluster.x-k8s.io"
            ],
            "generation": 2,
            "managedFields": [
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:annotations": {
                                ".": {},
                                "f:kubectl.kubernetes.io/last-applied-configuration": {}
                            }
                        },
                        "f:spec": {
                            ".": {},
                            "f:clusterNetwork": {
                                ".": {},
                                "f:pods": {
                                    ".": {},
                                    "f:cidrBlocks": {}
                                }
                            },
                            "f:controlPlaneRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            },
                            "f:infrastructureRef": {
                                ".": {},
                                "f:apiVersion": {},
                                "f:kind": {},
                                "f:name": {}
                            }
                        }
                    },
                    "manager": "kubectl-client-side-apply",
                    "operation": "Update",
                    "time": "2021-11-23T14:45:31Z"
                },
                {
                    "apiVersion": "cluster.x-k8s.io/v1beta1",
                    "fieldsType": "FieldsV1",
                    "fieldsV1": {
                        "f:metadata": {
                            "f:finalizers": {
                                ".": {},
                                "v:\"cluster.cluster.x-k8s.io\"": {}
                            }
                        },
                        "f:status": {
                            ".": {},
                            "f:conditions": {},
                            "f:observedGeneration": {},
                            "f:phase": {}
                        }
                    },
                    "manager": "manager",
                    "operation": "Update",
                    "time": "2021-11-23T14:46:01Z"
                }
            ],
            "name": "pop-9",
            "namespace": "default",
            "resourceVersion": "2296357",
            "uid": "727c8989-7d33-4037-bbb7-ab838d54f309"
        },
        "spec": {
            "clusterNetwork": {
                "pods": {
                    "cidrBlocks": [
                        "192.168.200.0/24"
                    ]
                }
            },
            "controlPlaneEndpoint": {
                "host": "",
                "port": 0
            },
            "controlPlaneRef": {
                "apiVersion": "controlplane.cluster.x-k8s.io/v1beta1",
                "kind": "KubeadmControlPlane",
                "name": "pop-9-control-plane",
                "namespace": "default"
            },
            "infrastructureRef": {
                "apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
                "kind": "AWSCluster",
                "name": "pop-9",
                "namespace": "default"
            }
        },
        "status": {
            "conditions": [
                {
                    "lastTransitionTime": "2021-11-23T14:46:01Z",
                    "message": "0 of 7 completed",
                    "reason": "VpcReconciliationFailed",
                    "severity": "Error",
                    "status": "False",
                    "type": "Ready"
                },
                {
                    "lastTransitionTime": "2021-11-23T14:46:01Z",
                    "message": "Waiting for control plane provider to indicate the control plane has been initialized",
                    "reason": "WaitingForControlPlaneProviderInitialized",
                    "severity": "Info",
                    "status": "False",
                    "type": "ControlPlaneInitialized"
                },
                {
                    "lastTransitionTime": "2021-11-23T15:28:59Z",
                    "reason": "Deleted",
                    "severity": "Info",
                    "status": "False",
                    "type": "ControlPlaneReady"
                },
                {
                    "lastTransitionTime": "2021-11-23T14:46:01Z",
                    "message": "0 of 7 completed",
                    "reason": "VpcReconciliationFailed",
                    "severity": "Error",
                    "status": "False",
                    "type": "InfrastructureReady"
                }
            ],
            "observedGeneration": 2,
            "phase": "Deleting"
        }
    }
]
"#;
