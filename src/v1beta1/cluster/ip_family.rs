use std::fmt;

use ipnet::IpNet;
use thiserror::Error;

use super::*;

/// ClusterIPFamily defines the types of supported IP families.
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd, Serialize, Deserialize)]
pub enum ClusterIpFamily {
    InvalidIpFamily,
    Ipv4IpFamily,
    Ipv6IpFamily,
    DualStackIpFamily,
}

impl Default for ClusterIpFamily {
    fn default() -> Self {
        Self::InvalidIpFamily
    }
}

impl ClusterIpFamily {
    pub fn to_str(&self) -> &'static str {
        match self {
            Self::InvalidIpFamily => "InvalidIPFamily",
            Self::Ipv4IpFamily => "IPv4IPFamily",
            Self::Ipv6IpFamily => "IPv6IPFamily",
            Self::DualStackIpFamily => "DualStackIPFamily",
        }
    }

    pub(crate) fn ip_family_for_cidr_strings(cidrs: &[String]) -> Result<Self, InvalidIpFamily> {
        let count = cidrs.len();
        if count > 2 {
            return Err(InvalidIpFamily::TooManyCidrs(count));
        }

        let families = cidrs
            .iter()
            .map(|text| text.parse())
            .collect::<Result<Vec<IpNet>, _>>()?
            .into();
        Ok(families)
    }
}

impl From<IpNet> for ClusterIpFamily {
    fn from(ipnet: IpNet) -> Self {
        match ipnet {
            IpNet::V4(_) => Self::Ipv4IpFamily,
            IpNet::V6(_) => Self::Ipv6IpFamily,
        }
    }
}

impl From<Vec<IpNet>> for ClusterIpFamily {
    fn from(nets: Vec<IpNet>) -> Self {
        let mut nets = nets.into_iter().map(Self::from);

        let first = nets.next();
        let second = nets.next();

        if let Some(first) = first {
            if let Some(second) = second {
                if first == second {
                    first
                } else {
                    Self::DualStackIpFamily
                }
            } else {
                first
            }
        } else {
            Self::Ipv4IpFamily
        }
    }
}

impl fmt::Display for ClusterIpFamily {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.to_str().fmt(f)
    }
}

#[derive(Debug, Error)]
pub enum InvalidIpFamily {
    #[error("Too many ({0}) CIDRs specified")]
    TooManyCidrs(usize),
    #[error("Could not parse CIDR: {0}")]
    InvalidCidr(#[from] ipnet::AddrParseError),
    #[error("Pods and Services IP Families mismatch")]
    IpFamilyMismatch,
}
