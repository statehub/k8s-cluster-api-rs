use super::*;

/// ClusterLabelName is the label set on machines linked to a cluster and
/// external objects(bootstrap and infrastructure providers).
pub const CLUSTER_LABEL_NAME: &str = "cluster.x-k8s.io/cluster-name";

// ClusterTopologyOwnedLabel is the label set on all the object which are managed as part of a ClusterTopology.
pub const CLUSTER_TOPOLOGY_OWNED_LABEL: &str = "topology.cluster.x-k8s.io/owned";

// ClusterTopologyMachineDeploymentLabelName is the label set on the generated  MachineDeployment objects
// to track the name of the MachineDeployment topology it represents.
pub const CLUSTER_TOPOLOGY_MACHINE_DEPLOYMENT_LABEL_NAME: &str =
    "topology.cluster.x-k8s.io/deployment-name";

// ProviderLabelName is the label set on components in the provider manifest.
// This label allows to easily identify all the components belonging to a provider; the clusterctl
// tool uses this label for implementing provider's lifecycle operations.
pub const PROVIDER_LABEL_NAME: &str = "cluster.x-k8s.io/provider";

// ClusterNameAnnotation is the annotation set on nodes identifying the name of the cluster the node belongs to.
pub const CLUSTER_NAME_ANNOTATION: &str = "cluster.x-k8s.io/cluster-name";

// ClusterNamespaceAnnotation is the annotation set on nodes identifying the namespace of the cluster the node belongs to.
pub const CLUSTER_NAMESPACE_ANNOTATION: &str = "cluster.x-k8s.io/cluster-namespace";

// MachineAnnotation is the annotation set on nodes identifying the machine the node belongs to.
pub const MACHINE_ANNOTATION: &str = "cluster.x-k8s.io/machine";

// OwnerKindAnnotation is the annotation set on nodes identifying the owner kind.
pub const OWNER_KIND_ANNOTATION: &str = "cluster.x-k8s.io/owner-kind";

// OwnerNameAnnotation is the annotation set on nodes identifying the owner name.
pub const OWNER_NAME_ANNOTATION: &str = "cluster.x-k8s.io/owner-name";

// PausedAnnotation is an annotation that can be applied to any Cluster API
// object to prevent a controller from processing a resource.
//
// Controllers working with Cluster API objects must check the existence of this annotation
// on the reconciled object.
pub const PAUSED_ANNOTATION: &str = "cluster.x-k8s.io/paused";

// DisableMachineCreate is an annotation that can be used to signal a MachineSet to stop creating new machines.
// It is utilized in the OnDelete MachineDeploymentStrategy to allow the MachineDeployment controller to scale down
// older MachineSets when Machines are deleted and add the new replicas to the latest MachineSet.
pub const DISABLE_MACHINE_CREATE: &str = "cluster.x-k8s.io/disable-machine-create";

// WatchLabel is a label othat can be applied to any Cluster API object.
//
// Controllers which allow for selective reconciliation may check this label and proceed
// with reconciliation of the object only if this label and a configured value is present.
pub const WATCH_LABEL: &str = "cluster.x-k8s.io/watch-filter";

// DeleteMachineAnnotation marks control plane and worker nodes that will be given priority for deletion
// when KCP or a machineset scales down. This annotation is given top priority on all delete policies.
pub const DELETE_MACHINE_ANNOTATION: &str = "cluster.x-k8s.io/delete-machine";

// TemplateClonedFromNameAnnotation is the infrastructure machine annotation that stores the name of the infrastructure template resource
// that was cloned for the machine. This annotation is set only during cloning a template. Older/adopted machines will not have this annotation.
pub const TEMPLATE_CLONED_FROM_NAME_ANNOTATION: &str = "cluster.x-k8s.io/cloned-from-name";

// TemplateClonedFromGroupKindAnnotation is the infrastructure machine annotation that stores the group-kind of the infrastructure template resource
// that was cloned for the machine. This annotation is set only during cloning a template. Older/adopted machines will not have this annotation.
pub const TEMPLATE_CLONED_FROM_GROUP_KIND_ANNOTATION: &str =
    "cluster.x-k8s.io/cloned-from-groupkind";

// MachineSkipRemediationAnnotation is the annotation used to mark the machines that should not be considered for remediation by MachineHealthCheck reconciler.
pub const MACHINE_SKIP_REMEDIATION_ANNOTATION: &str = "cluster.x-k8s.io/skip-remediation";

// ClusterSecretType defines the type of secret created by core components.
pub const CLUSTER_SECRET_TYPE: &str = "cluster.x-k8s.io/secret"; //nolint:gosec

// InterruptibleLabel is the label used to mark the nodes that run on interruptible instances.
pub const INTERRUPTIBLE_LABEL: &str = "cluster.x-k8s.io/interruptible";

// ManagedByAnnotation is an annotation that can be applied to InfraCluster resources to signify that
// some external system is managing the cluster infrastructure.
//
// Provider InfraCluster controllers will ignore resources with this annotation.
// An external controller must fulfill the contract of the InfraCluster resource.
// External infrastructure providers should ensure that the annotation, once set, cannot be removed.
pub const MANAGED_BY_ANNOTATION: &str = "cluster.x-k8s.io/managed-by";

// TemplateSuffix is the object kind suffix used by template types.
pub const TEMPLATE_SUFFIX: &str = "Template";

// var (
// 	// ZeroDuration is a zero value of the metav1.Duration type.
// 	ZeroDuration = metav1.Duration{}
// )

// MachineAddressType describes a valid MachineAddress type.
#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum MachineAddressType {
    #[serde(rename = "Hostname")]
    MachineHostName,
    #[serde(rename = "ExternalIP")]
    MachineExternalIP,
    #[serde(rename = "InternalIP")]
    MachineInternalIP,
    #[serde(rename = "ExternalDNS")]
    MachineExternalDNS,
    #[serde(rename = "InternalDNS")]
    MachineInternalDNS,
}

/// MachineAddress contains information for the node's address.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct MachineAddress {
    // Machine address type, one of Hostname, ExternalIP or InternalIP.
    pub r#type: MachineAddressType,

    // The machine address.
    pub address: String,
}

/// MachineAddresses is a slice of MachineAddress items to be used by infrastructure providers.
pub type MachineAddresses = Vec<MachineAddress>;

// ObjectMeta is metadata that all persisted resources must have, which includes all objects
// users must create. This is a copy of customizable fields from metav1.ObjectMeta.
//
// ObjectMeta is embedded in `Machine.Spec`, `MachineDeployment.Template` and `MachineSet.Template`,
// which are not top-level Kubernetes objects. Given that metav1.ObjectMeta has lots of special cases
// and read-only fields which end up in the generated CRD validation, having it as a subset simplifies
// the API and some issues that can impact user experience.
//
// During the [upgrade to controller-tools@v2](https://github.com/kubernetes-sigs/cluster-api/pull/1054)
// for v1alpha2, we noticed a failure would occur running Cluster API test suite against the new CRDs,
// specifically `spec.metadata.creationTimestamp in body must be of type string: "null"`.
// The investigation showed that `controller-tools@v2` behaves differently than its previous version
// when handling types from [metav1](k8s.io/apimachinery/pkg/apis/meta/v1) package.
//
// In more details, we found that embedded (non-top level) types that embedded `metav1.ObjectMeta`
// had validation properties, including for `creationTimestamp` (metav1.Time).
// The `metav1.Time` type specifies a custom json marshaller that, when IsZero() is true, returns `null`
// which breaks validation because the field isn't marked as nullable.
//
// In future versions, controller-tools@v2 might allow overriding the type and validation for embedded
// types. When that happens, this hack should be revisited.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct ObjectMeta {
    // Map of string keys and values that can be used to organize and categorize
    // (scope and select) objects. May match selectors of replication controllers
    // and services.
    // More info: http://kubernetes.io/docs/user-guide/labels
    // +optional
    pub labels: Option<BTreeMap<String, String>>,

    // Annotations is an unstructured key value map stored with a resource that may be
    // set by external tools to store and retrieve arbitrary metadata. They are not
    // queryable and should be preserved when modifying objects.
    // More info: http://kubernetes.io/docs/user-guide/annotations
    // +optional
    pub annotations: Option<BTreeMap<String, String>>,
}
