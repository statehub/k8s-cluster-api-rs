use super::*;

// ANCHOR: ConditionSeverity

// ConditionSeverity expresses the severity of a Condition Type failing.
#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum ConditionSeverity {
    // ConditionSeverityError specifies that a condition with `Status=False` is an error.
    Error,

    // ConditionSeverityWarning specifies that a condition with `Status=False` is a warning.
    Warning,

    // ConditionSeverityInfo specifies that a condition with `Status=False` is informative.
    Info,

    // ConditionSeverityNone should apply only to conditions with `Status=True`.
    #[serde(rename = "")]
    None,
}

// ANCHOR_END: ConditionSeverity

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum ConditionStatus {
    True,
    False,
    Unknown,
}

// ANCHOR: ConditionType

// ConditionType is a valid value for Condition.Type.
pub type ConditionType = String;

// ANCHOR_END: ConditionType

// ANCHOR: Condition

// Condition defines an observation of a Cluster API resource operational state.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Condition {
    // Type of condition in CamelCase or in foo.example.com/CamelCase.
    // Many .condition.type values are consistent across resources like Available, but because arbitrary conditions
    // can be useful (see .node.status.conditions), the ability to deconflict is important.
    pub r#type: ConditionType,

    // Status of the condition, one of True, False, Unknown.
    pub status: ConditionStatus,

    // Severity provides an explicit classification of Reason code, so the users or machines can immediately
    // understand the current situation and act accordingly.
    // The Severity field MUST be set only when Status=False.
    // +optional
    pub severity: Option<ConditionSeverity>,

    // Last time the condition transitioned from one status to another.
    // This should be when the underlying condition changed. If that is not known, then using the time when
    // the API field changed is acceptable.
    pub last_transition_time: metav1::Time,

    // The reason for the condition's last transition in CamelCase.
    // The specific API may choose whether or not this field is considered a guaranteed API.
    // This field may not be empty.
    // +optional
    pub reason: Option<String>,

    // A human readable message indicating details about the transition.
    // This field may be empty.
    // +optional
    pub message: Option<String>,
}

// ANCHOR_END: Condition

// ANCHOR: Conditions

// Conditions provide observations of the operational state of a Cluster API resource.
pub type Conditions = Vec<Condition>;

// ANCHOR_END: Conditions
