use super::*;

pub mod impls;

// +kubebuilder:resource:path=machinedeployments,shortName=md,scope=Namespaced,categories=cluster-api

/// MachineDeploymentSpec defines the desired state of MachineDeployment.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize, CustomResource)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "cluster.x-k8s.io",
    version = "v1beta1",
    kind = "MachineDeployment",
    plural = "machinedeployments",
    shortname = "md",
    status = "MachineDeploymentStatus"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct MachineDeploymentSpec {
    /// ClusterName is the name of the Cluster this object belongs to.
    // +kubebuilder:validation:MinLength=1
    pub cluster_name: String, // `json:"clusterName"`

    /// Number of desired machines. Defaults to 1.
    /// This is a pointer to distinguish between explicit zero and not specified.
    // +optional
    // +kubebuilder:default=1
    pub replicas: Option<i32>, // `json:"replicas,omitempty"`

    /// Label selector for machines. Existing MachineSets whose machines are
    /// selected by this will be the ones affected by this deployment.
    /// It must match the machine template's labels.
    pub selector: metav1::LabelSelector, // `json:"selector"`

    /// Template describes the machines that will be created.
    pub template: MachineTemplateSpec, // `json:"template"`

    /// The deployment strategy to use to replace existing machines with
    /// new ones.
    // +optional
    pub strategy: Option<MachineDeploymentStrategy>, // `json:"strategy,omitempty"`

    /// Minimum number of seconds for which a newly created machine should
    /// be ready.
    /// Defaults to 0 (machine will be considered available as soon as it
    // /is ready)
    // +optional
    pub min_ready_seconds: Option<i32>, // `json:"minReadySeconds,omitempty"`

    /// The number of old MachineSets to retain to allow rollback.
    /// This is a pointer to distinguish between explicit zero and not specified.
    /// Defaults to 1.
    // +optional
    pub revision_history_limit: Option<i32>, // `json:"revisionHistoryLimit,omitempty"`

    // Indicates that the deployment is paused.
    // +optional
    pub paused: Option<bool>, // `json:"paused,omitempty"`

    // The maximum time in seconds for a deployment to make progress before it
    // is considered to be failed. The deployment controller will continue to
    // process failed deployments and a condition with a ProgressDeadlineExceeded
    // reason will be surfaced in the deployment status. Note that progress will
    // not be estimated during the time a deployment is paused. Defaults to 600s.
    // +optional
    pub progress_deadline_seconds: Option<i32>, // `json:"progressDeadlineSeconds,omitempty"`
}

#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MachineDeploymentStatus {
    /// The generation observed by the deployment controller.
    // +optional
    observed_generation: Option<i64>, // `json:"observedGeneration,omitempty"`

    /// Selector is the same as the label selector but in the string format to avoid introspection
    /// by clients. The string will be in the same format as the query-param syntax.
    /// More info about label selectors: http://kubernetes.io/docs/user-guide/labels#label-selectors
    // +optional
    selector: Option<String>, // `json:"selector,omitempty"`

    /// Total number of non-terminated machines targeted by this deployment
    /// (their labels match the selector).
    // +optional
    replicas: Option<i32>, //`json:"replicas"`

    /// Total number of non-terminated machines targeted by this deployment
    /// that have the desired template spec.
    // +optional
    updated_replicas: Option<i32>, // `json:"updatedReplicas"`

    /// Total number of ready machines targeted by this deployment.
    // +optional
    ready_replicas: Option<i32>, // `json:"readyReplicas"`

    /// Total number of available machines (ready for at least minReadySeconds)
    /// targeted by this deployment.
    // +optional
    available_replicas: Option<i32>, // `json:"availableReplicas"`

    /// Total number of unavailable machines targeted by this deployment.
    /// This is the total number of machines that are still required for
    /// the deployment to have 100% available capacity. They may either
    /// be machines that are running but not yet available or machines
    /// that still have not been created.
    // +optional
    unavailable_replicas: Option<i32>, // `json:"unavailableReplicas"`

    /// Phase represents the current phase of a MachineDeployment (ScalingUp, ScalingDown, Running, Failed, or Unknown).
    // +optional
    phase: Option<String>, // `json:"phase,omitempty"`

    /// Conditions defines current service state of the MachineDeployment.
    // +optional
    conditions: Option<Conditions>, // `json:"conditions,omitempty"`
}

/// MachineDeploymentStrategyType defines the type of MachineDeployment rollout strategies.
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum MachineDeploymentStrategyType {
    /// RollingUpdateMachineDeploymentStrategyType replaces the old MachineSet by new one using rolling update
    /// i.e. gradually scale down the old MachineSet and scale up the new one.
    RollingUpdate,

    /// OnDeleteMachineDeploymentStrategyType replaces old MachineSets when the deletion of the associated machines are completed.
    OnDelete,
}

/// MachineDeploymentStrategy describes how to replace existing machines
/// with new ones.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MachineDeploymentStrategy {
    /// Type of deployment.
    /// Default is RollingUpdate.
    // +kubebuilder:validation:Enum=RollingUpdate;OnDelete
    // +optional
    pub r#type: Option<MachineDeploymentStrategyType>, // `json:"type,omitempty"`

    /// Rolling update config params. Present only if
    /// MachineDeploymentStrategyType = RollingUpdate.
    // +optional
    pub rolling_update: Option<MachineRollingUpdateDeployment>, // `json:"rollingUpdate,omitempty"`
}

// MachineRollingUpdateDeployment is used to control the desired behavior of rolling update.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MachineRollingUpdateDeployment {
    // The maximum number of machines that can be unavailable during the update.
    // Value can be an absolute number (ex: 5) or a percentage of desired
    // machines (ex: 10%).
    // Absolute number is calculated from percentage by rounding down.
    // This can not be 0 if MaxSurge is 0.
    // Defaults to 0.
    // Example: when this is set to 30%, the old MachineSet can be scaled
    // down to 70% of desired machines immediately when the rolling update
    // starts. Once new machines are ready, old MachineSet can be scaled
    // down further, followed by scaling up the new MachineSet, ensuring
    // that the total number of machines available at all times
    // during the update is at least 70% of desired machines.
    // +optional
    pub max_unavailable: Option<intstr::IntOrString>, // `json:"maxUnavailable,omitempty"`

    // The maximum number of machines that can be scheduled above the
    // desired number of machines.
    // Value can be an absolute number (ex: 5) or a percentage of
    // desired machines (ex: 10%).
    // This can not be 0 if MaxUnavailable is 0.
    // Absolute number is calculated from percentage by rounding up.
    // Defaults to 1.
    // Example: when this is set to 30%, the new MachineSet can be scaled
    // up immediately when the rolling update starts, such that the total
    // number of old and new machines do not exceed 130% of desired
    // machines. Once old machines have been killed, new MachineSet can
    // be scaled up further, ensuring that total number of machines running
    // at any time during the update is at most 130% of desired machines.
    // +optional
    pub max_surge: Option<intstr::IntOrString>, // `json:"maxSurge,omitempty"`

    // DeletePolicy defines the policy used by the MachineDeployment to identify nodes to delete when downscaling.
    // Valid values are "Random, "Newest", "Oldest"
    // When no value is supplied, the default DeletePolicy of MachineSet is used
    // +kubebuilder:validation:Enum=Random;Newest;Oldest
    // +optional
    pub delete_policy: Option<String>, // `json:"deletePolicy,omitempty"`
}

/*

package v1beta1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/util/intstr"
)

const (
    // MachineDeploymentTopologyFinalizer is the finalizer used by the topology MachineDeployment controller to
    // clean up referenced template resources if necessary when a MachineDeployment is being deleted.
    MachineDeploymentTopologyFinalizer = "machinedeployment.topology.cluster.x-k8s.io"
)

const (
    // RevisionAnnotation is the revision annotation of a machine deployment's machine sets which records its rollout sequence.
    RevisionAnnotation = "machinedeployment.clusters.x-k8s.io/revision"

    // RevisionHistoryAnnotation maintains the history of all old revisions that a machine set has served for a machine deployment.
    RevisionHistoryAnnotation = "machinedeployment.clusters.x-k8s.io/revision-history"

    // DesiredReplicasAnnotation is the desired replicas for a machine deployment recorded as an annotation
    // in its machine sets. Helps in separating scaling events from the rollout process and for
    // determining if the new machine set for a deployment is really saturated.
    DesiredReplicasAnnotation = "machinedeployment.clusters.x-k8s.io/desired-replicas"

    // MaxReplicasAnnotation is the maximum replicas a deployment can have at a given point, which
    // is machinedeployment.spec.replicas + maxSurge. Used by the underlying machine sets to estimate their
    // proportions in case the deployment has surge replicas.
    MaxReplicasAnnotation = "machinedeployment.clusters.x-k8s.io/max-replicas"

    // MachineDeploymentUniqueLabel is the label applied to Machines
    // in a MachineDeployment containing the hash of the template.
    MachineDeploymentUniqueLabel = "machine-template-hash"
)

// ANCHOR: MachineDeploymentSpec


// ANCHOR_END: MachineDeploymentSpec

// ANCHOR: MachineDeploymentStrategy

// ANCHOR_END: MachineDeploymentStrategy

// ANCHOR: MachineRollingUpdateDeployment

// ANCHOR_END: MachineRollingUpdateDeployment

// ANCHOR: MachineDeploymentStatus

// MachineDeploymentStatus defines the observed state of MachineDeployment.
type MachineDeploymentStatus struct {
    // The generation observed by the deployment controller.
    // +optional
    ObservedGeneration int64 `json:"observedGeneration,omitempty"`

    // Selector is the same as the label selector but in the string format to avoid introspection
    // by clients. The string will be in the same format as the query-param syntax.
    // More info about label selectors: http://kubernetes.io/docs/user-guide/labels#label-selectors
    // +optional
    Selector string `json:"selector,omitempty"`

    // Total number of non-terminated machines targeted by this deployment
    // (their labels match the selector).
    // +optional
    Replicas int32 `json:"replicas"`

    // Total number of non-terminated machines targeted by this deployment
    // that have the desired template spec.
    // +optional
    UpdatedReplicas int32 `json:"updatedReplicas"`

    // Total number of ready machines targeted by this deployment.
    // +optional
    ReadyReplicas int32 `json:"readyReplicas"`

    // Total number of available machines (ready for at least minReadySeconds)
    // targeted by this deployment.
    // +optional
    AvailableReplicas int32 `json:"availableReplicas"`

    // Total number of unavailable machines targeted by this deployment.
    // This is the total number of machines that are still required for
    // the deployment to have 100% available capacity. They may either
    // be machines that are running but not yet available or machines
    // that still have not been created.
    // +optional
    UnavailableReplicas int32 `json:"unavailableReplicas"`

    // Phase represents the current phase of a MachineDeployment (ScalingUp, ScalingDown, Running, Failed, or Unknown).
    // +optional
    Phase string `json:"phase,omitempty"`

    // Conditions defines current service state of the MachineDeployment.
    // +optional
    Conditions Conditions `json:"conditions,omitempty"`
}

// ANCHOR_END: MachineDeploymentStatus

// MachineDeploymentPhase indicates the progress of the machine deployment.
type MachineDeploymentPhase string

const (
    // MachineDeploymentPhaseScalingUp indicates the MachineDeployment is scaling up.
    MachineDeploymentPhaseScalingUp = MachineDeploymentPhase("ScalingUp")

    // MachineDeploymentPhaseScalingDown indicates the MachineDeployment is scaling down.
    MachineDeploymentPhaseScalingDown = MachineDeploymentPhase("ScalingDown")

    // MachineDeploymentPhaseRunning indicates scaling has completed and all Machines are running.
    MachineDeploymentPhaseRunning = MachineDeploymentPhase("Running")

    // MachineDeploymentPhaseFailed indicates there was a problem scaling and user intervention might be required.
    MachineDeploymentPhaseFailed = MachineDeploymentPhase("Failed")

    // MachineDeploymentPhaseUnknown indicates the state of the MachineDeployment cannot be determined.
    MachineDeploymentPhaseUnknown = MachineDeploymentPhase("Unknown")
)

// SetTypedPhase sets the Phase field to the string representation of MachineDeploymentPhase.
func (md *MachineDeploymentStatus) SetTypedPhase(p MachineDeploymentPhase) {
    md.Phase = string(p)
}

// GetTypedPhase attempts to parse the Phase field and return
// the typed MachineDeploymentPhase representation.
func (md *MachineDeploymentStatus) GetTypedPhase() MachineDeploymentPhase {
    switch phase := MachineDeploymentPhase(md.Phase); phase {
    case
        MachineDeploymentPhaseScalingDown,
        MachineDeploymentPhaseScalingUp,
        MachineDeploymentPhaseRunning,
        MachineDeploymentPhaseFailed:
        return phase
    default:
        return MachineDeploymentPhaseUnknown
    }
}

// +kubebuilder:object:root=true
// +kubebuilder:resource:path=machinedeployments,shortName=md,scope=Namespaced,categories=cluster-api
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector
// +kubebuilder:printcolumn:name="Cluster",type="string",JSONPath=".spec.clusterName",description="Cluster"
// +kubebuilder:printcolumn:name="Replicas",type="integer",JSONPath=".status.replicas",description="Total number of non-terminated machines targeted by this MachineDeployment"
// +kubebuilder:printcolumn:name="Ready",type="integer",JSONPath=".status.readyReplicas",description="Total number of ready machines targeted by this MachineDeployment"
// +kubebuilder:printcolumn:name="Updated",type=integer,JSONPath=".status.updatedReplicas",description="Total number of non-terminated machines targeted by this deployment that have the desired template spec"
// +kubebuilder:printcolumn:name="Unavailable",type=integer,JSONPath=".status.unavailableReplicas",description="Total number of unavailable machines targeted by this MachineDeployment"
// +kubebuilder:printcolumn:name="Phase",type="string",JSONPath=".status.phase",description="MachineDeployment status such as ScalingUp/ScalingDown/Running/Failed/Unknown"
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Time duration since creation of MachineDeployment"
// +kubebuilder:printcolumn:name="Version",type="string",JSONPath=".spec.template.spec.version",description="Kubernetes version associated with this MachineDeployment"

// MachineDeployment is the Schema for the machinedeployments API.
type MachineDeployment struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec   MachineDeploymentSpec   `json:"spec,omitempty"`
    Status MachineDeploymentStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// MachineDeploymentList contains a list of MachineDeployment.
type MachineDeploymentList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []MachineDeployment `json:"items"`
}

func init() {
    SchemeBuilder.Register(&MachineDeployment{}, &MachineDeploymentList{})
}

// GetConditions returns the set of conditions for the machinedeployment.
func (m *MachineDeployment) GetConditions() Conditions {
    return m.Status.Conditions
}

// SetConditions updates the set of conditions on the machinedeployment.
func (m *MachineDeployment) SetConditions(conditions Conditions) {
    m.Status.Conditions = conditions
}

*/

#[cfg(test)]
mod tests;
