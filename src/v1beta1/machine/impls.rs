use super::*;

impl MachineSpec {
    pub fn new(cluster_name: &str) -> Self {
        let cluster_name = cluster_name.to_string();
        Self {
            cluster_name,
            ..Self::default()
        }
    }
}
