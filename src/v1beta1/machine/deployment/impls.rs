use super::*;

impl MachineDeployment {
    pub fn with_cluster_name(name: &str, cluster_name: &str) -> Self {
        let spec = MachineDeploymentSpec::new(cluster_name).replicas(3);
        Self::new(name, spec)
    }

    #[must_use]
    pub fn machine_template<T>(mut self, machine_template: &T) -> Self
    where
        T: MachineTemplate,
    {
        let infrastructure_ref = machine_template.object_ref();
        let mut spec = self.spec.template.spec.take().unwrap_or_default();
        spec.infrastructure_ref = infrastructure_ref;
        self.spec.template.spec = Some(spec);
        self
    }

    #[must_use]
    pub fn version(mut self, version: &str) -> Self {
        let mut spec = self.spec.template.spec.take().unwrap_or_default();
        spec.version = Some(version.to_string());
        self.spec.template.spec = Some(spec);
        self
    }

    #[must_use]
    pub fn bootstrap<T>(mut self, template: &T) -> Self
    where
        T: ControlPlaneConfigTemplate,
    {
        self.spec
            .template
            .spec
            .get_or_insert_with(Default::default)
            .bootstrap
            .config_ref = Some(template.object_ref());
        self
    }
}

impl MachineDeploymentSpec {
    pub fn new(cluster_name: &str) -> Self {
        let spec = MachineSpec::new(cluster_name);
        let template = MachineTemplateSpec {
            spec: Some(spec),
            ..MachineTemplateSpec::default()
        };
        let cluster_name = cluster_name.to_string();
        Self {
            cluster_name,
            template,
            // replicas: todo!(),
            // selector: todo!(),
            // strategy: todo!(),
            // min_ready_seconds: todo!(),
            // revision_history_limit: todo!(),
            // paused: todo!(),
            // progress_deadline_seconds: todo!(),
            ..Self::default()
        }
    }

    #[must_use]
    pub fn replicas(mut self, replicas: i32) -> Self {
        self.replicas = Some(replicas);
        self
    }

    #[must_use]
    pub fn template(mut self, template: MachineTemplateSpec) -> Self {
        self.template = template;
        self
    }
}
