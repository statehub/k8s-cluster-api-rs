use super::*;

use kube::ResourceExt;
use serde_json as json;

#[test]
fn de() {
    let machine_deployment: MachineDeployment = json::from_str(MACHINEDEPLOYMENT).unwrap();

    assert_eq!(machine_deployment.name(), "pop-2-worker");

    let spec = machine_deployment.spec;

    assert_eq!(spec.cluster_name, "pop-2");
}

const MACHINEDEPLOYMENT: &str = r#"{"apiVersion":"cluster.x-k8s.io/v1beta1","kind":"MachineDeployment","metadata":{"annotations":{"machinedeployment.clusters.x-k8s.io/revision":"1"},"creationTimestamp":"2021-12-09T18:52:16Z","generation":2,"labels":{"cluster.x-k8s.io/cluster-name":"pop-2"},"managedFields":[{"apiVersion":"cluster.x-k8s.io/v1beta1","fieldsType":"FieldsV1","fieldsV1":{"f:spec":{".":{},"f:clusterName":{},"f:replicas":{},"f:selector":{},"f:template":{".":{},"f:spec":{".":{},"f:bootstrap":{".":{},"f:configRef":{".":{},"f:apiVersion":{},"f:kind":{},"f:name":{},"f:namespace":{}}},"f:clusterName":{},"f:infrastructureRef":{".":{},"f:apiVersion":{},"f:kind":{},"f:name":{},"f:namespace":{}},"f:version":{}}}}},"manager":"unknown","operation":"Update","time":"2021-12-09T19:09:08Z"},{"apiVersion":"cluster.x-k8s.io/v1beta1","fieldsType":"FieldsV1","fieldsV1":{"f:metadata":{"f:annotations":{".":{},"f:machinedeployment.clusters.x-k8s.io/revision":{}},"f:ownerReferences":{".":{},"k:{\"uid\":\"d9562710-0bec-47d6-94fc-74087595bc35\"}":{".":{},"f:apiVersion":{},"f:kind":{},"f:name":{},"f:uid":{}}}},"f:status":{".":{},"f:conditions":{},"f:observedGeneration":{},"f:phase":{},"f:replicas":{},"f:selector":{},"f:unavailableReplicas":{},"f:updatedReplicas":{}}},"manager":"manager","operation":"Update","time":"2021-12-09T19:09:11Z"}],"name":"pop-2-worker","namespace":"default","ownerReferences":[{"apiVersion":"cluster.x-k8s.io/v1beta1","kind":"Cluster","name":"pop-2","uid":"d9562710-0bec-47d6-94fc-74087595bc35"}],"resourceVersion":"11416062","uid":"c09bdf54-be37-42c4-ac0c-067fad84eb60"},"spec":{"clusterName":"pop-2","minReadySeconds":0,"progressDeadlineSeconds":600,"replicas":3,"revisionHistoryLimit":1,"selector":{"matchLabels":{"cluster.x-k8s.io/cluster-name":"pop-2","cluster.x-k8s.io/deployment-name":"pop-2-worker"}},"strategy":{"rollingUpdate":{"maxSurge":1,"maxUnavailable":0},"type":"RollingUpdate"},"template":{"metadata":{"labels":{"cluster.x-k8s.io/cluster-name":"pop-2","cluster.x-k8s.io/deployment-name":"pop-2-worker"}},"spec":{"bootstrap":{"configRef":{"apiVersion":"bootstrap.cluster.x-k8s.io/v1beta1","kind":"KubeadmConfigTemplate","name":"pop-2-worker","namespace":"default"}},"clusterName":"pop-2","infrastructureRef":{"apiVersion":"infrastructure.cluster.x-k8s.io/v1beta1","kind":"AWSMachineTemplate","name":"pop-2-worker","namespace":"default"},"version":"v1.21.6"}}},"status":{"conditions":[{"lastTransitionTime":"2021-12-09T19:09:10Z","message":"Minimum availability requires 3 replicas, current 0 available","reason":"WaitingForAvailableMachines","severity":"Warning","status":"False","type":"Ready"},{"lastTransitionTime":"2021-12-09T19:09:10Z","message":"Minimum availability requires 3 replicas, current 0 available","reason":"WaitingForAvailableMachines","severity":"Warning","status":"False","type":"Available"}],"observedGeneration":2,"phase":"ScalingUp","replicas":3,"selector":"cluster.x-k8s.io/cluster-name=pop-2,cluster.x-k8s.io/deployment-name=pop-2-worker","unavailableReplicas":3,"updatedReplicas":3}}"#;
