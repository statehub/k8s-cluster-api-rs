use maplit::{btreemap, convert_args};

use super::*;

impl KubeadmConfigTemplate {
    pub fn aws(name: &str) -> Self {
        let spec = KubeadmConfigTemplateSpec {
            template: KubeadmConfigTemplateResource {
                spec: KubeadmConfigSpec {
                    join_configuration: Some(JoinConfiguration {
                        node_registration: Some(NodeRegistrationOptions {
                            kubelet_extra_args: convert_args!(btreemap!("cloud-provider" => "aws")),
                            name: Some("{{ ds.meta_data.local_hostname }}".to_string()),
                            ..Default::default()
                        }),
                        ..Default::default()
                    }),
                    ..Default::default()
                },
            },
        };
        Self::new(name, spec)
    }

    #[must_use]
    pub fn namespace(self, namespace: &str) -> Self {
        Self {
            metadata: kube::core::ObjectMeta {
                namespace: Some(namespace.to_string()),
                ..self.metadata
            },
            ..self
        }
    }
}
