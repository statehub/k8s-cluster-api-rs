use super::*;

mod impls;

/// KubeadmConfigTemplateSpec defines the desired state of KubeadmConfigTemplate.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize, CustomResource)]
#[serde(rename_all = "camelCase")]
#[kube(
    group = "bootstrap.cluster.x-k8s.io",
    version = "v1beta1",
    kind = "KubeadmConfigTemplate",
    plural = "kubeadmconfigtemplates"
)]
#[kube(namespaced)]
#[kube(schema = "disabled")]
pub struct KubeadmConfigTemplateSpec {
    pub template: KubeadmConfigTemplateResource, // `json:"template"`
}

/// KubeadmConfigTemplateResource defines the Template structure.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct KubeadmConfigTemplateResource {
    pub spec: KubeadmConfigSpec, // `json:"spec,omitempty"`
}

/*

package v1beta1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:object:root=true
// +kubebuilder:resource:path=kubeadmconfigtemplates,scope=Namespaced,categories=cluster-api
// +kubebuilder:storageversion
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Time duration since creation of KubeadmConfigTemplate"

// KubeadmConfigTemplate is the Schema for the kubeadmconfigtemplates API.
type KubeadmConfigTemplate struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec KubeadmConfigTemplateSpec `json:"spec,omitempty"`
}

// +kubebuilder:object:root=true

// KubeadmConfigTemplateList contains a list of KubeadmConfigTemplate.
type KubeadmConfigTemplateList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []KubeadmConfigTemplate `json:"items"`
}

func init() {
    SchemeBuilder.Register(&KubeadmConfigTemplate{}, &KubeadmConfigTemplateList{})
}
*/
