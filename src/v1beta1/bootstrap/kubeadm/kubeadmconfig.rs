use super::*;

pub use template::{KubeadmConfigTemplate, KubeadmConfigTemplateSpec};

mod template;

/// KubeadmConfigSpec defines the desired state of KubeadmConfig.
/// Either ClusterConfiguration and InitConfiguration should be defined or the JoinConfiguration should be defined.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
// #[kube(
//     group = "infrastructure.cluster.x-k8s.io",
//     version = "v1beta1",
//     kind = "AWSCluster",
//     plural = "awsclusters",
//     status = "AWSClusterStatus"
// )]
// #[kube(namespaced)]
// #[kube(schema = "disabled")]
pub struct KubeadmConfigSpec {
    /// ClusterConfiguration along with InitConfiguration are the configurations necessary for the init command
    // +optional
    pub cluster_configuration: Option<ClusterConfiguration>, // `json:"clusterConfiguration,omitempty"`

    // InitConfiguration along with ClusterConfiguration are the configurations necessary for the init command
    // +optional
    pub init_configuration: Option<InitConfiguration>, // `json:"initConfiguration,omitempty"`

    /// JoinConfiguration is the kubeadm configuration for the join command
    // +optional
    pub join_configuration: Option<JoinConfiguration>, // `json:"joinConfiguration,omitempty"`

    /// Files specifies extra files to be passed to user_data upon creation.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub files: Vec<File>, // `json:"files,omitempty"`

    /// DiskSetup specifies options for the creation of partition tables and file systems on devices.
    // +optional
    pub disk_setup: Option<DiskSetup>, // `json:"diskSetup,omitempty"`

    /// Mounts specifies a list of mount points to be setup.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub mounts: Vec<MountPoints>, // `json:"mounts,omitempty"`

    /// PreKubeadmCommands specifies extra commands to run before kubeadm runs
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub pre_kubeadm_commands: Vec<String>, // `json:"preKubeadmCommands,omitempty"`

    /// PostKubeadmCommands specifies extra commands to run after kubeadm runs
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub post_kubeadm_commands: Vec<String>, // `json:"postKubeadmCommands,omitempty"`

    /// Users specifies extra users to add
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub users: Vec<User>, // `json:"users,omitempty"`

    /// NTP specifies NTP configuration
    // +optional
    pub ntp: Option<Ntp>, // `json:"ntp,omitempty"`

    /// Format specifies the output format of the bootstrap data
    // +optional
    pub format: Option<Format>, // `json:"format,omitempty"`

    // Verbosity is the number for the kubeadm log level verbosity.
    // It overrides the `--v` flag in kubeadm commands.
    // +optional
    pub verbosity: Option<i32>, // `json:"verbosity,omitempty"`

    // UseExperimentalRetryJoin replaces a basic kubeadm command with a shell
    // script with retries for joins.
    //
    // This is meant to be an experimental temporary workaround on some environments
    // where joins fail due to timing (and other issues). The long term goal is to add retries to
    // kubeadm proper and use that functionality.
    //
    // This will add about 40KB to userdata
    //
    // For more information, refer to https://github.com/kubernetes-sigs/cluster-api/pull/2763#discussion_r397306055.
    // +optional
    pub use_experimental_retry_join: Option<bool>, // `json:"useExperimentalRetryJoin,omitempty"`
}

/// File defines the input for generating write_files in cloud-init.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct File {
    /// Path specifies the full path on disk where to store the file.
    pub path: String, // `json:"path"`

    /// Owner specifies the ownership of the file, e.g. "root:root".
    // +optional
    pub owner: Option<String>, // `json:"owner,omitempty"`

    /// Permissions specifies the permissions to assign to the file, e.g. "0640".
    // +optional
    pub permissions: Option<String>, // `json:"permissions,omitempty"`

    /// Encoding specifies the encoding of the file contents.
    // +optional
    pub encoding: Option<Encoding>, // `json:"encoding,omitempty"`

    /// Content is the actual content of the file.
    // +optional
    pub content: Option<String>, // `json:"content,omitempty"`

    /// ContentFrom is a referenced source of content to populate the file.
    // +optional
    pub content_from: Option<FileSource>, // `json:"contentFrom,omitempty"`
}

/// DiskSetup defines input for generated disk_setup and fs_setup in cloud-init.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DiskSetup {
    /// Partitions specifies the list of the partitions to setup.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub partitions: Vec<Partition>, // `json:"partitions,omitempty"`

    /// Filesystems specifies the list of file systems to setup.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub filesystems: Vec<Filesystem>, // `json:"filesystems,omitempty"`
}

/// MountPoints defines input for generated mounts in cloud-init.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MountPoints(Vec<String>);

/// User defines the input for a generated user in cloud-init.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct User {
    /// Name specifies the user name
    pub name: String, // `json:"name"`

    /// Gecos specifies the gecos to use for the user
    // +optional
    pub gecos: Option<String>, // `json:"gecos,omitempty"`

    // Groups specifies the additional groups for the user
    // +optional
    pub groups: Option<String>, // `json:"groups,omitempty"`

    // HomeDir specifies the home directory to use for the user
    // +optional
    pub home_dir: Option<String>, // `json:"homeDir,omitempty"`

    // Inactive specifies whether to mark the user as inactive
    // +optional
    pub inactive: Option<bool>, // `json:"inactive,omitempty"`

    // Shell specifies the user's shell
    // +optional
    pub shell: Option<String>, // `json:"shell,omitempty"`

    // Passwd specifies a hashed password for the user
    // +optional
    pub passwd: Option<String>, // `json:"passwd,omitempty"`

    // PrimaryGroup specifies the primary group for the user
    // +optional
    pub primary_group: Option<String>, // `json:"primaryGroup,omitempty"`

    // LockPassword specifies if password login should be disabled
    // +optional
    pub lock_password: Option<bool>, // `json:"lockPassword,omitempty"`

    // Sudo specifies a sudo role for the user
    // +optional
    pub sudo: Option<String>, // `json:"sudo,omitempty"`

    // SSHAuthorizedKeys specifies a list of ssh authorized keys for the user
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub ssh_authorized_keys: Vec<String>, // `json:"sshAuthorizedKeys,omitempty"`
}

/// NTP defines input for generated ntp in cloud-init.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Ntp {
    /// Servers specifies which NTP servers to use
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub servers: Vec<String>, // `json:"servers,omitempty"`

    /// Enabled specifies whether NTP should be enabled
    // +optional
    pub enabled: Option<bool>, // `json:"enabled,omitempty"`
}

/// Format specifies the output format of the bootstrap data
// +kubebuilder:validation:Enum=cloud-config
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Format {
    #[serde(rename = "cloud-config")]
    CloudConfig,
}

/// Encoding specifies the cloud-init file encoding.
// +kubebuilder:validation:Enum=base64;gzip;gzip+base64
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Encoding {
    /// Base64 implies the contents of the file are encoded as base64.
    #[serde(rename = "base64")]
    Base64,
    /// Gzip implies the contents of the file are encoded with gzip.
    #[serde(rename = "gzip")]
    Gzip,
    /// GzipBase64 implies the contents of the file are first base64 encoded and then gzip encoded.
    #[serde(rename = "gzip+base64")]
    GzipBase64,
}

/// FileSource is a union of all possible external source types for file data.
/// Only one field may be populated in any given instance. Developers adding new
/// sources of data for target systems should add them here.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FileSource {
    /// Secret represents a secret that should populate this file.
    pub secret: SecretFileSource, // `json:"secret"`
}

/// SecretFileSource adapts a Secret into a FileSource.
///
/// The contents of the target Secret's Data field will be presented
/// as files using the keys in the Data field as the file names.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SecretFileSource {
    /// Name of the secret in the KubeadmBootstrapConfig's namespace to use.
    pub name: String, // `json:"name"`

    /// Key is the key in the secret's data map for this value.
    pub key: String, // `json:"key"`
}

/// Partition defines how to create and layout a partition.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Partition {
    /// Device is the name of the device.
    pub device: String, // `json:"device"`
    /// Layout specifies the device layout.
    /// If it is true, a single partition will be created for the entire device.
    /// When layout is false, it means don't partition or ignore existing partitioning.
    pub layout: bool, // `json:"layout"`
    /// Overwrite describes whether to skip checks and create the partition if a partition or filesystem is found on the device.
    /// Use with caution. Default is 'false'.
    // +optional
    pub overwrite: Option<bool>, // `json:"overwrite,omitempty"`
    /// TableType specifies the tupe of partition table. The following are supported:
    /// 'mbr': default and setups a MS-DOS partition table
    /// 'gpt': setups a GPT partition table
    // +optional
    pub table_type: Option<String>, // `json:"tableType,omitempty"`
}

// Filesystem defines the file systems to be created.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Filesystem {
    /// Device specifies the device name
    pub device: String, // `json:"device"`
    /// Filesystem specifies the file system type.
    pub filesystem: String, // `json:"filesystem"`
    /// Label specifies the file system label to be used. If set to None, no label is used.
    pub label: String, // `json:"label"`
    /// Partition specifies the partition to use. The valid options are: "auto|any", "auto", "any", "none", and <NUM>, where NUM is the actual partition number.
    // +optional
    pub partition: Option<String>, // `json:"partition,omitempty"`
    /// Overwrite defines whether or not to overwrite any existing filesystem.
    /// If true, any pre-existing file system will be destroyed. Use with Caution.
    // +optional
    pub overwrite: Option<bool>, // `json:"overwrite,omitempty"`
    /// ReplaceFS is a special directive, used for Microsoft Azure that instructs cloud-init to replace a file system of <FS_TYPE>.
    /// NOTE: unless you define a label, this requires the use of the 'any' partition directive.
    // +optional
    #[serde(rename = "replaceFS")]
    pub replace_fs: Option<String>, // `json:"replaceFS,omitempty"`
    // ExtraOpts defined extra options to add to the command for creating the file system.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub extra_opts: Vec<String>, // `json:"extraOpts,omitempty"`
}

/* =========
package v1beta1

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)



// KubeadmConfigStatus defines the observed state of KubeadmConfig.
type KubeadmConfigStatus struct {
    // Ready indicates the BootstrapData field is ready to be consumed
    // +optional
    Ready bool `json:"ready"`

    // DataSecretName is the name of the secret that stores the bootstrap data script.
    // +optional
    DataSecretName *string `json:"dataSecretName,omitempty"`

    // FailureReason will be set on non-retryable errors
    // +optional
    FailureReason string `json:"failureReason,omitempty"`

    // FailureMessage will be set on non-retryable errors
    // +optional
    FailureMessage string `json:"failureMessage,omitempty"`

    // ObservedGeneration is the latest generation observed by the controller.
    // +optional
    ObservedGeneration int64 `json:"observedGeneration,omitempty"`

    // Conditions defines current service state of the KubeadmConfig.
    // +optional
    Conditions clusterv1.Conditions `json:"conditions,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:resource:path=kubeadmconfigs,scope=Namespaced,categories=cluster-api
// +kubebuilder:storageversion
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Cluster",type="string",JSONPath=".metadata.labels['cluster\\.x-k8s\\.io/cluster-name']",description="Cluster"
// +kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Time duration since creation of KubeadmConfig"

// KubeadmConfig is the Schema for the kubeadmconfigs API.
type KubeadmConfig struct {
    metav1.TypeMeta   `json:",inline"`
    metav1.ObjectMeta `json:"metadata,omitempty"`

    Spec   KubeadmConfigSpec   `json:"spec,omitempty"`
    Status KubeadmConfigStatus `json:"status,omitempty"`
}

// GetConditions returns the set of conditions for this object.
func (c *KubeadmConfig) GetConditions() clusterv1.Conditions {
    return c.Status.Conditions
}

// SetConditions sets the conditions on this object.
func (c *KubeadmConfig) SetConditions(conditions clusterv1.Conditions) {
    c.Status.Conditions = conditions
}

// +kubebuilder:object:root=true

// KubeadmConfigList contains a list of KubeadmConfig.
type KubeadmConfigList struct {
    metav1.TypeMeta `json:",inline"`
    metav1.ListMeta `json:"metadata,omitempty"`
    Items           []KubeadmConfig `json:"items"`
}

func init() {
    SchemeBuilder.Register(&KubeadmConfig{}, &KubeadmConfigList{})
}






========= */
