use super::*;

pub use kubeadmconfig::KubeadmConfigSpec;
pub use kubeadmconfig::KubeadmConfigTemplate;
pub use kubeadmconfig::KubeadmConfigTemplateSpec;

mod kubeadmconfig;

/// InitConfiguration contains a list of elements that is specific "kubeadm init"-only runtime
/// information.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InitConfiguration {
    #[serde(flatten)]
    pub meta: TypeMeta,

    /// BootstrapTokens is respected at `kubeadm init` time and describes a set of Bootstrap Tokens to create.
    /// This information IS NOT uploaded to the kubeadm cluster configmap, partly because of its sensitive nature
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub bootstrap_tokens: Vec<BootstrapToken>, // `json:"bootstrapTokens,omitempty"`

    /// NodeRegistration holds fields that relate to registering the new control-plane node to the cluster.
    /// When used in the context of control plane nodes, NodeRegistration should remain consistent
    /// across both InitConfiguration and JoinConfiguration
    // +optional
    pub node_registration: Option<NodeRegistrationOptions>, // `json:"nodeRegistration,omitempty"`

    // LocalAPIEndpoint represents the endpoint of the API server instance that's deployed on this control plane node
    // In HA setups, this differs from ClusterConfiguration.ControlPlaneEndpoint in the sense that ControlPlaneEndpoint
    // is the global endpoint for the cluster, which then loadbalances the requests to each individual API server. This
    // configuration object lets you customize what IP/DNS name and port the local API server advertises it's accessible
    // on. By default, kubeadm tries to auto-detect the IP of the default interface and use that, but in case that process
    // fails you may set the desired value here.
    // +optional
    #[serde(rename = "localAPIEndpoint")]
    pub local_api_endpoint: Option<APIEndpoint>, // `json:"localAPIEndpoint,omitempty"`
}

// ClusterConfiguration contains cluster-wide configuration for a kubeadm cluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ClusterConfiguration {
    #[serde(flatten)]
    pub meta: TypeMeta,

    /// Etcd holds configuration for etcd.
    /// NB: This value defaults to a Local (stacked) etcd
    // +optional
    pub etcd: Option<Etcd>, // `json:"etcd,omitempty"`

    /// Networking holds configuration for the networking topology of the cluster.
    /// NB: This value defaults to the Cluster object spec.clusterNetwork.
    // +optional
    pub networking: Option<Networking>, // `json:"networking,omitempty"`

    // KubernetesVersion is the target version of the control plane.
    // NB: This value defaults to the Machine object spec.version
    // +optional
    pub kubernetes_version: Option<String>, // `json:"kubernetesVersion,omitempty"`

    /// ControlPlaneEndpoint sets a stable IP address or DNS name for the control plane; it
    /// can be a valid IP address or a RFC-1123 DNS subdomain, both with optional TCP port.
    /// In case the ControlPlaneEndpoint is not specified, the AdvertiseAddress + BindPort
    /// are used; in case the ControlPlaneEndpoint is specified but without a TCP port,
    /// the BindPort is used.
    /// Possible usages are:
    /// e.g. In a cluster with more than one control plane instances, this field should be
    /// assigned the address of the external load balancer in front of the
    /// control plane instances.
    /// e.g.  in environments with enforced node recycling, the ControlPlaneEndpoint
    /// could be used for assigning a stable DNS to the control plane.
    /// NB: This value defaults to the first value in the Cluster object status.apiEndpoints array.
    // +optional
    pub control_plane_endpoint: Option<String>, // `json:"controlPlaneEndpoint,omitempty"`

    /// APIServer contains extra settings for the API server control plane component
    // +optional
    pub api_server: Option<APIServer>, // `json:"apiServer,omitempty"`

    /// ControllerManager contains extra settings for the controller manager control plane component
    // +optional
    pub controller_manager: Option<ControlPlaneComponent>, // `json:"controllerManager,omitempty"`

    /// Scheduler contains extra settings for the scheduler control plane component
    // +optional
    pub scheduler: Option<ControlPlaneComponent>, // `json:"scheduler,omitempty"`

    /// DNS defines the options for the DNS add-on installed in the cluster.
    // +optional
    pub dns: Option<DNS>, // `json:"dns,omitempty"`

    /// CertificatesDir specifies where to store or look for all required certificates.
    /// NB: if not provided, this will default to `/etc/kubernetes/pki`
    // +optional
    pub certificates_dir: Option<String>, // `json:"certificatesDir,omitempty"`

    /// ImageRepository sets the container registry to pull images from.
    /// If empty, `k8s.gcr.io` will be used by default; in case of kubernetes version is a CI build (kubernetes version starts with `ci/` or `ci-cross/`)
    /// `gcr.io/k8s-staging-ci-images` will be used as a default for control plane components and for kube-proxy, while `k8s.gcr.io`
    /// will be used for all the other images.
    // +optional
    pub image_repository: Option<String>, // `json:"imageRepository,omitempty"`

    /// FeatureGates enabled by the user.
    // +optional
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub feature_gates: BTreeMap<String, bool>, // `json:"featureGates,omitempty"`

    /// The cluster name
    // +optional
    pub cluster_name: Option<String>, // `json:"clusterName,omitempty"`
}

/// JoinConfiguration contains elements describing a particular node.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoinConfiguration {
    #[serde(flatten)]
    pub meta: TypeMeta, // metav1.TypeMeta `json:",inline"`

    /// NodeRegistration holds fields that relate to registering the new control-plane node to the cluster.
    /// When used in the context of control plane nodes, NodeRegistration should remain consistent
    /// across both InitConfiguration and JoinConfiguration
    // +optional
    pub node_registration: Option<NodeRegistrationOptions>, // `json:"nodeRegistration,omitempty"`

    /// CACertPath is the path to the SSL certificate authority used to
    /// secure comunications between node and control-plane.
    /// Defaults to "/etc/kubernetes/pki/ca.crt".
    // +optional
    // TODO: revisit when there is defaulting from k/k
    pub ca_cert_path: Option<String>, // `json:"caCertPath,omitempty"`

    /// Discovery specifies the options for the kubelet to use during the TLS Bootstrap process
    // +optional
    // TODO: revisit when there is defaulting from k/k
    pub discovery: Option<Discovery>, // `json:"discovery,omitempty"`

    /// ControlPlane defines the additional control plane instance to be deployed on the joining node.
    /// If nil, no additional control plane instance will be deployed.
    // +optional
    pub control_plane: Option<JoinControlPlane>, // `json:"controlPlane,omitempty"`
}

/// BootstrapToken describes one bootstrap token, stored as a Secret in the cluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BootstrapToken {
    /// Token is used for establishing bidirectional trust between nodes and control-planes.
    /// Used for joining nodes in the cluster.
    pub token: String, // `json:"token"`
    /// Description sets a human-friendly message why this token exists and what it's used
    /// for, so other administrators can know its purpose.
    // +optional
    pub description: Option<String>, // `json:"description,omitempty"`
    /// TTL defines the time to live for this token. Defaults to 24h.
    /// Expires and TTL are mutually exclusive.
    // +optional
    // pub ttl: *metav1.Duration `json:"ttl,omitempty"`
    pub ttl: Option<i64>, // `json:"ttl,omitempty"`
    /// Expires specifies the timestamp when this token expires. Defaults to being set
    /// dynamically at runtime based on the TTL. Expires and TTL are mutually exclusive.
    // +optional
    pub expires: Option<metav1::Time>, // `json:"expires,omitempty"`
    /// Usages describes the ways in which this token can be used. Can by default be used
    /// for establishing bidirectional trust, but that can be changed here.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub usages: Vec<String>, // `json:"usages,omitempty"`
    /// Groups specifies the extra groups that this token will authenticate as when/if
    /// used for authentication
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub groups: Vec<String>, // `json:"groups,omitempty"`
}

/// NodeRegistrationOptions holds fields that relate to registering a new control-plane or node to the cluster, either via "kubeadm init" or "kubeadm join".
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NodeRegistrationOptions {
    /// Name is the `.Metadata.Name` field of the Node API object that will be created in this `kubeadm init` or `kubeadm join` operation.
    /// This field is also used in the CommonName field of the kubelet's client certificate to the API server.
    /// Defaults to the hostname of the node if not provided.
    // +optional
    pub name: Option<String>, // `json:"name,omitempty"`

    /// CRISocket is used to retrieve container runtime info. This information will be annotated to the Node API object, for later re-use
    // +optional
    pub cri_socket: Option<String>, // `json:"criSocket,omitempty"`

    /// Taints specifies the taints the Node API object should be registered with. If this field is unset, i.e. nil, in the `kubeadm init` process
    /// it will be defaulted to []v1.Taint{'node-role.kubernetes.io/master=""'}. If you don't want to taint your control-plane node, set this field to an
    /// empty slice, i.e. `taints: {}` in the YAML file. This field is solely used for Node registration.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub taints: Vec<corev1::Taint>, // `json:"taints,omitempty"`

    /// KubeletExtraArgs passes through extra arguments to the kubelet. The arguments here are passed to the kubelet command line via the environment file
    /// kubeadm writes at runtime for the kubelet to source. This overrides the generic base-level configuration in the kubelet-config-1.X ConfigMap
    /// Flags have higher priority when parsing. These values are local and specific to the node kubeadm is executing on.
    // +optional
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub kubelet_extra_args: BTreeMap<String, String>, // `json:"kubeletExtraArgs,omitempty"`

    /// IgnorePreflightErrors provides a slice of pre-flight errors to be ignored when the current node is registered.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub ignore_preflight_errors: Vec<String>, // `json:"ignorePreflightErrors,omitempty"`
}

// APIEndpoint struct contains elements of API server instance deployed on a node.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct APIEndpoint {
    /// AdvertiseAddress sets the IP address for the API server to advertise.
    // +optional
    pub advertise_address: Option<String>, // `json:"advertiseAddress,omitempty"`

    /// BindPort sets the secure port for the API Server to bind to.
    /// Defaults to 6443.
    // +optional
    pub bind_port: Option<i32>, // `json:"bindPort,omitempty"`
}

/// Etcd contains elements describing Etcd configuration.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Etcd {
    /// Local provides configuration knobs for configuring the local etcd instance
    /// Local and External are mutually exclusive
    // +optional
    pub local: Option<LocalEtcd>, // `json:"local,omitempty"`

    // External describes how to connect to an external etcd cluster
    // Local and External are mutually exclusive
    // +optional
    pub external: Option<ExternalEtcd>, // `json:"external,omitempty"`
}

/// LocalEtcd describes that kubeadm should run an etcd cluster locally.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocalEtcd {
    /// ImageMeta allows to customize the container used for etcd
    #[serde(flatten)]
    pub image_meta: ImageMeta, // `json:",inline"`

    /// DataDir is the directory etcd will place its data.
    /// Defaults to "/var/lib/etcd".
    // +optional
    pub data_dir: Option<String>, // `json:"dataDir,omitempty"`

    /// ExtraArgs are extra arguments provided to the etcd binary
    /// when run inside a static pod.
    // +optional
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub extra_args: BTreeMap<String, String>, // `json:"extraArgs,omitempty"`

    /// ServerCertSANs sets extra Subject Alternative Names for the etcd server signing cert.
    // +optional
    #[serde(
        rename = "serverCertSANs",
        default,
        skip_serializing_if = "Vec::is_empty"
    )]
    pub server_cert_sans: Vec<String>, // `json:"serverCertSANs,omitempty"`
    /// PeerCertSANs sets extra Subject Alternative Names for the etcd peer signing cert.
    // +optional
    #[serde(
        rename = "peerCertSANs",
        default,
        skip_serializing_if = "Vec::is_empty"
    )]
    pub peer_cert_sans: Vec<String>, // `json:"peerCertSANs,omitempty"`
}

/// ExternalEtcd describes an external etcd cluster.
/// Kubeadm has no knowledge of where certificate files live and they must be supplied.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExternalEtcd {
    /// Endpoints of etcd members. Required for ExternalEtcd.
    pub endpoints: Vec<String>, // `json:"endpoints"`

    /// CAFile is an SSL Certificate Authority file used to secure etcd communication.
    /// Required if using a TLS connection.
    pub ca_file: String, // `json:"caFile"`

    /// CertFile is an SSL certification file used to secure etcd communication.
    /// Required if using a TLS connection.
    pub cert_file: String, // `json:"certFile"`

    /// KeyFile is an SSL key file used to secure etcd communication.
    /// Required if using a TLS connection.
    pub key_file: String, // `json:"keyFile"`
}

/// ImageMeta allows to customize the image used for components that are not
/// originated from the Kubernetes/Kubernetes release process.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ImageMeta {
    /// ImageRepository sets the container registry to pull images from.
    /// if not set, the ImageRepository defined in ClusterConfiguration will be used instead.
    // +optional
    pub image_repository: Option<String>, // `json:"imageRepository,omitempty"`

    /// ImageTag allows to specify a tag for the image.
    /// In case this value is set, kubeadm does not change automatically the version of the above components during upgrades.
    // +optional
    pub image_tag: Option<String>, // `json:"imageTag,omitempty"`

                                   //TODO: evaluate if we need also a ImageName based on user feedbacks
}

/// Networking contains elements describing cluster's networking configuration.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Networking {
    // ServiceSubnet is the subnet used by k8s services.
    // Defaults to a comma-delimited string of the Cluster object's spec.clusterNetwork.pods.cidrBlocks, or
    // to "10.96.0.0/12" if that's unset.
    // +optional
    pub service_subnet: Option<String>, // `json:"serviceSubnet,omitempty"`
    // PodSubnet is the subnet used by pods.
    // If unset, the API server will not allocate CIDR ranges for every node.
    // Defaults to a comma-delimited string of the Cluster object's spec.clusterNetwork.services.cidrBlocks if that is set
    // +optional
    pub pod_subnet: Option<String>, // `json:"podSubnet,omitempty"`
    // DNSDomain is the dns domain used by k8s services. Defaults to "cluster.local".
    // +optional
    pub dns_domain: Option<String>, // `json:"dnsDomain,omitempty"`
}

/// ControlPlaneComponent holds settings common to control plane component of the cluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ControlPlaneComponent {
    /// ExtraArgs is an extra set of flags to pass to the control plane component.
    /// TODO: This is temporary and ideally we would like to switch all components to
    /// use ComponentConfig + ConfigMaps.
    // +optional
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub extra_args: BTreeMap<String, String>, // `json:"extraArgs,omitempty"`

    /// ExtraVolumes is an extra set of host volumes, mounted to the control plane component.
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub extra_volumes: Vec<HostPathMount>, // `json:"extraVolumes,omitempty"`
}

// APIServer holds settings necessary for API server deployments in the cluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct APIServer {
    #[serde(flatten)]
    pub control_plane: ControlPlaneComponent, // `json:",inline"`

    /// CertSANs sets extra Subject Alternative Names for the API Server signing cert.
    // +optional
    #[serde(rename = "certSANs", default, skip_serializing_if = "Vec::is_empty")]
    pub cert_sans: Vec<String>, // `json:"certSANs,omitempty"`

    // TimeoutForControlPlane controls the timeout that we use for API server to appear
    // +optional
    pub timeout_for_control_plane: Option<i64>, // metav1.Duration `json:"timeoutForControlPlane,omitempty"`
}

// DNS defines the DNS addon that should be used in the cluster.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DNS {
    /// ImageMeta allows to customize the image used for the DNS component
    #[serde(flatten)]
    pub image_meta: ImageMeta, // ImageMeta `json:",inline"`
}

/// Discovery specifies the options for the kubelet to use during the TLS Bootstrap process.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Discovery {
    /// BootstrapToken is used to set the options for bootstrap token based discovery
    /// BootstrapToken and File are mutually exclusive
    // +optional
    pub bootstrap_token: Option<BootstrapTokenDiscovery>, // `json:"bootstrapToken,omitempty"`

    /// File is used to specify a file or URL to a kubeconfig file from which to load cluster information
    /// BootstrapToken and File are mutually exclusive
    // +optional
    pub file: Option<FileDiscovery>, // `json:"file,omitempty"`

    /// TLSBootstrapToken is a token used for TLS bootstrapping.
    /// If .BootstrapToken is set, this field is defaulted to .BootstrapToken.Token, but can be overridden.
    /// If .File is set, this field **must be set** in case the KubeConfigFile does not contain any other authentication information
    // +optional
    pub tls_bootstrap_token: Option<String>, // `json:"tlsBootstrapToken,omitempty"`

    /// Timeout modifies the discovery timeout
    // +optional
    pub timeout: Option<i64>, // *metav1.Duration `json:"timeout,omitempty"`
}

/// JoinControlPlane contains elements describing an additional control plane instance to be deployed on the joining node.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoinControlPlane {
    /// LocalAPIEndpoint represents the endpoint of the API server instance to be deployed on this node.
    // +optional
    #[serde(rename = "localAPIEndpoint")]
    pub local_api_endpoint: Option<APIEndpoint>, // `json:"localAPIEndpoint,omitempty"`
}

/// HostPathMount contains elements describing volumes that are mounted from the
/// host.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HostPathMount {
    /// Name of the volume inside the pod template.
    pub name: String, // `json:"name"`
    /// HostPath is the path in the host that will be mounted inside
    /// the pod.
    pub host_path: String, // `json:"hostPath"`
    /// MountPath is the path inside the pod where hostPath will be mounted.
    pub mount_path: String, // `json:"mountPath"`
    /// ReadOnly controls write access to the volume
    // +optional
    pub read_only: Option<bool>, // `json:"readOnly,omitempty"`
    /// PathType is the type of the HostPath.
    // +optional
    pub path_type: Option<String>, // corev1.HostPathType> `json:"pathType,omitempty"`
}

/// FileDiscovery is used to specify a file or URL to a kubeconfig file from which to load cluster information.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FileDiscovery {
    /// KubeConfigPath is used to specify the actual file path or URL to the kubeconfig file from which to load cluster information
    pub kube_config_path: String, // `json:"kubeConfigPath"`
}

/// BootstrapTokenDiscovery is used to set the options for bootstrap token based discovery.
#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BootstrapTokenDiscovery {
    /// Token is a token used to validate cluster information
    /// fetched from the control-plane.
    pub token: String, // `json:"token"`

    /// APIServerEndpoint is an IP or domain name to the API server from which info will be fetched.
    // +optional
    pub api_server_endpoint: Option<String>, // `json:"apiServerEndpoint,omitempty"`

    /// CACertHashes specifies a set of public key pins to verify
    /// when token-based discovery is used. The root CA found during discovery
    /// must match one of these values. Specifying an empty set disables root CA
    /// pinning, which can be unsafe. Each hash is specified as "<type>:<value>",
    /// where the only currently supported type is "sha256". This is a hex-encoded
    /// SHA-256 hash of the Subject Public Key Info (SPKI) object in DER-encoded
    /// ASN.1. These hashes can be calculated using, for example, OpenSSL:
    /// openssl x509 -pubkey -in ca.crt openssl rsa -pubin -outform der 2>&/dev/null | openssl dgst -sha256 -hex
    // +optional
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub ca_cert_hashes: Vec<String>, // `json:"caCertHashes,omitempty"`

    // UnsafeSkipCAVerification allows token-based discovery
    // without CA verification via CACertHashes. This can weaken
    // the security of kubeadm since other nodes can impersonate the control-plane.
    // +optional
    #[serde(rename = "unsafeSkipCAVerification")]
    pub unsafe_skip_ca_verification: Option<bool>, // `json:"unsafeSkipCAVerification,omitempty"`
}

/*

package v1beta1

import (
    "fmt"
    "strings"

    "github.com/pkg/errors"
    corev1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    bootstrapapi "k8s.io/cluster-bootstrap/token/api"
    bootstraputil "k8s.io/cluster-bootstrap/token/util"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object



// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// ClusterStatus contains the cluster status. The ClusterStatus will be stored in the kubeadm-config
// ConfigMap in the cluster, and then updated by kubeadm when additional control plane instance joins or leaves the cluster.
// Deprecated: ClusterStatus has been removed from kubeadm v1beta3 API; This type is preserved only to support
// conversion to older versions of the kubeadm API.
type ClusterStatus struct {
    metav1.TypeMeta `json:",inline"`

    // APIEndpoints currently available in the cluster, one for each control plane/api server instance.
    // The key of the map is the IP of the host's default interface
    APIEndpoints map[string]APIEndpoint `json:"apiEndpoints"`
}



// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object



// BootstrapTokenString is a token of the format abcdef.abcdef0123456789 that is used
// for both validation of the practically of the API server from a joining node's point
// of view and as an authentication method for the node in the bootstrap phase of
// "kubeadm join". This token is and should be short-lived.
//
// +kubebuilder:validation:Type=string
type BootstrapTokenString struct {
    ID     string `json:"-"`
    Secret string `json:"-"`
}

// MarshalJSON implements the json.Marshaler interface.
func (bts BootstrapTokenString) MarshalJSON() ([]byte, error) {
    return []byte(fmt.Sprintf(`"%s"`, bts.String())), nil
}

// UnmarshalJSON implements the json.Unmarshaller interface.
func (bts *BootstrapTokenString) UnmarshalJSON(b []byte) error {
    // If the token is represented as "", just return quickly without an error
    if len(b) == 0 {
        return nil
    }

    // Remove unnecessary " characters coming from the JSON parser
    token := strings.ReplaceAll(string(b), `"`, ``)
    // Convert the string Token to a BootstrapTokenString object
    newbts, err := NewBootstrapTokenString(token)
    if err != nil {
        return err
    }
    bts.ID = newbts.ID
    bts.Secret = newbts.Secret
    return nil
}

// String returns the string representation of the BootstrapTokenString.
func (bts BootstrapTokenString) String() string {
    if len(bts.ID) > 0 && len(bts.Secret) > 0 {
        return bootstraputil.TokenFromIDAndSecret(bts.ID, bts.Secret)
    }
    return ""
}

// NewBootstrapTokenString converts the given Bootstrap Token as a string
// to the BootstrapTokenString object used for serialization/deserialization
// and internal usage. It also automatically validates that the given token
// is of the right format.
func NewBootstrapTokenString(token string) (*BootstrapTokenString, error) {
    substrs := bootstraputil.BootstrapTokenRegexp.FindStringSubmatch(token)
    // TODO: Add a constant for the 3 value here, and explain better why it's needed (other than because how the regexp parsin works)
    if len(substrs) != 3 {
        return nil, errors.Errorf("the bootstrap token %q was not of the form %q", token, bootstrapapi.BootstrapTokenPattern)
    }

    return &BootstrapTokenString{ID: substrs[1], Secret: substrs[2]}, nil
}

*/
