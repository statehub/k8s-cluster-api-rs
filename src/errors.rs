use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Clone, Debug, Deserialize, Serialize, Error)]
pub enum MachineStatusError {
    /// InvalidConfigurationMachineError represents that the combination
    /// of configuration in the MachineSpec is not supported by this cluster.
    /// This is not a transient error, but
    /// indicates a state that must be fixed before progress can be made.
    ///
    /// Example: the ProviderSpec specifies an instance type that doesn't exist,.

    #[error("InvalidConfiguration")]
    InvalidConfiguration, // MachineError MachineStatusError = "InvalidConfiguration"

    // UnsupportedChangeMachineError indicates that the MachineSpec has been updated in a way that
    // is not supported for reconciliation on this cluster. The spec may be
    // completely valid from a configuration standpoint, but the controller
    // does not support changing the real world state to match the new
    // spec.
    //
    // Example: the responsible controller is not capable of changing the
    // container runtime from docker to rkt.
    #[error("UnsupportedChange")]
    UnsupportedChange, // MachineError MachineStatusError = "UnsupportedChange"

    // InsufficientResourcesMachineError generally refers to exceeding one's quota in a cloud provider,
    // or running out of physical machines in an on-premise environment.
    #[error("InsufficientResources")]
    InsufficientResources, // MachineError MachineStatusError = "InsufficientResources"

    // CreateMachineError indicates an error while trying to create a Node to match this
    // Machine. This may indicate a transient problem that will be fixed
    // automatically with time, such as a service outage, or a terminal
    // error during creation that doesn't match a more specific
    // MachineStatusError value.
    //
    // Example: timeout trying to connect to GCE.
    #[error("CreateMachineError")]
    CreateError, // MachineStatusError = "CreateError"

    // UpdateMachineError indicates an error while trying to update a Node that this
    // Machine represents. This may indicate a transient problem that will be
    // fixed automatically with time, such as a service outage,
    //
    // Example: error updating load balancers.
    #[error("UpdateMachineError")]
    UpdateError, // MachineStatusError = "UpdateError"

    // DeleteMachineError indicates an error was encountered while trying to delete the Node that this
    // Machine represents. This could be a transient or terminal error, but
    // will only be observable if the provider's Machine controller has
    // added a finalizer to the object to more gracefully handle deletions.
    //
    // Example: cannot resolve EC2 IP address.
    #[error("DeleteMachineError")]
    DeleteError, // MachineStatusError = "DeleteError"

    // JoinClusterTimeoutMachineError indicates that the machine did not join the cluster
    // as a new node within the expected timeframe after instance
    // creation at the provider succeeded
    //
    // Example use case: A controller that deletes Machines which do
    // not result in a Node joining the cluster within a given timeout
    // and that are managed by a MachineSet.
    #[error("JoinClusterTimeoutMachineError")]
    JoinClusterTimeoutError, // = "JoinClusterTimeoutError",
}

/* ***********************

// MachineStatusError defines errors states for Machine objects.
type MachineStatusError string

// Constants aren't automatically generated for unversioned packages.
// Instead share the same constant for all versioned packages.

const (
)

*********************** */

// ClusterStatusError defines errors states for Cluster objects.
#[derive(Clone, Debug, Error, Serialize, Deserialize)]
pub enum ClusterStatusError {
    // InvalidConfigurationClusterError indicates that the cluster
    // configuration is invalid.
    #[error("InvalidConfiguration")]
    InvalidConfigurationClusterError,

    // UnsupportedChangeClusterError indicates that the cluster
    // spec has been updated in an unsupported way. That cannot be
    // reconciled.
    #[error("UnsupportedChange")]
    UnsupportedChangeClusterError,

    // CreateClusterError indicates that an error was encountered
    // when trying to create the cluster.
    #[error("CreateError")]
    CreateClusterError,

    // UpdateClusterError indicates that an error was encountered
    // when trying to update the cluster.
    #[error("UpdateError")]
    UpdateClusterError,

    // DeleteClusterError indicates that an error was encountered
    // when trying to delete the cluster.
    #[error("DeleteError")]
    DeleteClusterError,
}

// KubeadmControlPlaneStatusError defines errors states for KubeadmControlPlane objects.
#[derive(Clone, Debug, Error, Serialize, Deserialize)]
pub enum KubeadmControlPlaneStatusError {
    /// InvalidConfigurationKubeadmControlPlaneError indicates that the kubeadm control plane
    /// configuration is invalid.
    #[error("InvalidConfiguration")]
    InvalidConfiguration,

    /// UnsupportedChangeKubeadmControlPlaneError indicates that the kubeadm control plane
    /// spec has been updated in an unsupported way that cannot be
    /// reconciled.
    #[error("UnsupportedChange")]
    UnsupportedChange,

    /// CreateKubeadmControlPlaneError indicates that an error was encountered
    /// when trying to create the kubeadm control plane.
    #[error("CreateError")]
    CreateError,

    /// UpdateKubeadmControlPlaneError indicates that an error was encountered
    /// when trying to update the kubeadm control plane.
    #[error("UpdateError")]
    UpdateError,

    /// DeleteKubeadmControlPlaneError indicates that an error was encountered
    /// when trying to delete the kubeadm control plane.
    #[error("DeleteError")]
    DeleteError,
}

/* ***********************
// MachineSetStatusError defines errors states for MachineSet objects.
type MachineSetStatusError string

const (
    // InvalidConfigurationMachineSetError represents
    // the combination of configuration in the MachineTemplateSpec
    // is not supported by this cluster. This is not a transient error, but
    // indicates a state that must be fixed before progress can be made.
    //
    // Example: the ProviderSpec specifies an instance type that doesn't exist.
    InvalidConfigurationMachineSetError MachineSetStatusError = "InvalidConfiguration"
)

// MachinePoolStatusFailure defines errors states for MachinePool objects.
type MachinePoolStatusFailure string

const (
    // InvalidConfigurationMachinePoolError represemts
    // the combination of configuration in the MachineTemplateSpec
    // is not supported by this cluster. This is not a transient error, but
    // indicates a state that must be fixed before progress can be made.
    //
    // Example: the ProviderSpec specifies an instance type that doesn't exist.
    InvalidConfigurationMachinePoolError MachinePoolStatusFailure = "InvalidConfiguration"
)

*********************** */
