use super::*;

use bootstrap::kubeadm as cabpkv1;
use cluster as clusterv1;

pub use cluster::ClusterIpFamily;
pub use cluster::ClusterPhase;
pub use cluster::InvalidIpFamily;
pub use cluster::*;

pub use bootstrap::kubeadm::KubeadmConfigSpec;
pub use bootstrap::kubeadm::KubeadmConfigTemplate;
pub use controlplane::kubeadm::KubeadmControlPlane;
pub use infrastructure::aws::AWSCluster;
pub use infrastructure::aws::AWSMachineTemplate;
pub use machine::deployment::MachineDeployment;

use crate::errors as capierrors;

pub mod bootstrap;
pub mod cluster;
pub mod controlplane;
pub mod infrastructure;
pub mod machine;

mod impls;

#[derive(Debug)]
pub enum ControlPlane {
    Kubeadm(KubeadmControlPlane),
}

pub trait ControlPlaneConfigTemplate {
    fn object_ref(&self) -> corev1::ObjectReference;
}

#[derive(Debug)]
pub enum Infrastructure {
    Aws(AWSCluster),
}

pub trait MachineTemplate {
    fn object_ref(&self) -> corev1::ObjectReference;
}
